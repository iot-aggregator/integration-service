﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Transaction;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Extensions;
using IntegrationService.Infrastructure.Persistence.Models;

namespace IntegrationService.Infrastructure.Persistence.Repositories
{
    public class SqlRaspberryRepository : IRaspberryRepository
    {
        private readonly ISqlConnectionFactory _sqlConnectionFactory;

        public SqlRaspberryRepository(ISqlConnectionFactory sqlConnectionFactory)
        {
            _sqlConnectionFactory = sqlConnectionFactory;
        }
        
        /// <summary>
        ///   Default transaction exception handler with rollback and default DbException handler
        /// </summary>
        private Func<DbException, DbTransaction, Task> DefaultTransactionExceptionHandler
            => async (ex, transaction) =>
            {
                await transaction.RollbackAsync();
                await DefaultExceptionHandler(ex);
            };

        /// <summary>
        ///   Default DbException handler that throws exception
        /// </summary>
        private Func<DbException, Task> DefaultExceptionHandler
            => ex => throw ex;

        /// <summary>
        ///   Adds DeviceController using specified transaction
        /// </summary>
        /// <param name="deviceController">DeviceController to be added</param>
        /// <param name="transaction">Transaction in which entity will be added</param>
        /// <returns>Task</returns>
        private async Task AddDeviceControllerAsync(DeviceController deviceController,
            IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                @"INSERT INTO devices_on_pins(id, rpi_id, pin_name, status, device_type, device_name, description, waiting_time_sec, action_time_sec) 
                        VALUES (@Id, @RaspberryId, @PinName, @PinStatus::STATUS, @DeviceType::DEVICE_TYPE, @DeviceName, @Description, @WaitingTimeInSeconds, @ActionTimeInSeconds);",
                new
                {
                    deviceController.Id,
                    deviceController.RaspberryId,
                    deviceController.PinName,
                    PinStatus = deviceController.PinStatus.ToString().ToLower(),
                    DeviceController.DeviceType,
                    DeviceName = deviceController.Name,
                    deviceController.Description,
                    WaitingTimeInSeconds = deviceController.WaitingTime.TotalSeconds,
                    ActionTimeInSeconds = deviceController.ActionTime.TotalSeconds
                }
            );

        /// <summary>
        ///   Updates DeviceController using specified transaction
        /// </summary>
        /// <param name="deviceController">DeviceController to be updated</param>
        /// <param name="transaction">Transaction in which entity will be updated</param>
        /// <returns>Task</returns>
        private async Task UpdateDeviceControllerAsync(DeviceController deviceController,
            IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                @"UPDATE devices_on_pins
                    SET
                        pin_name = @PinName,
                        status = @PinStatus::STATUS,
                        device_name = @DeviceName,
                        description = @Description,
                        waiting_time_sec = @WaitingTimeInSeconds,
                        action_time_sec = @ActionTimeInSeconds
                    WHERE id = @Id;",
                new
                {
                    deviceController.Id,
                    deviceController.PinName,
                    PinStatus = deviceController.PinStatus.ToString().ToLower(),
                    DeviceName = deviceController.Name,
                    deviceController.Description,
                    WaitingTimeInSeconds = deviceController.WaitingTime.TotalSeconds,
                    ActionTimeInSeconds = deviceController.ActionTime.TotalSeconds
                });

        /// <summary>
        ///   Adds Sensor using specified transaction
        /// </summary>
        /// <param name="sensor">Sensor to be added</param>
        /// <param name="transaction">Transaction in which entity will be added</param>
        /// <returns>Task</returns>
        private async Task AddSensorAsync(Sensor sensor, IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                @"INSERT INTO devices_on_pins(id, rpi_id, pin_name, status, device_type, device_name, description, measurement_type, sensor_type, coefficient_a, coefficient_b) 
                        VALUES (@Id, @RaspberryId, @PinName, @PinStatus::STATUS, @DeviceType::DEVICE_TYPE, @DeviceName, @Description, @MeasurementType, @SensorType::SENSOR_TYPE, @CoefficientA, @CoefficientB);",
                new
                {
                    sensor.Id,
                    sensor.RaspberryId,
                    sensor.PinName,
                    PinStatus = sensor.PinStatus.ToString().ToLower(),
                    Sensor.DeviceType,
                    DeviceName = sensor.Name,
                    sensor.Description,
                    sensor.MeasurementType,
                    SensorType = sensor.Type.ToString().ToLower(),
                    sensor.CoefficientA,
                    sensor.CoefficientB
                }
            );

        /// <summary>
        ///   Updates Sensor using specified transaction
        /// </summary>
        /// <param name="sensor">Sensor to be updated</param>
        /// <param name="transaction">Transaction in which entity will be updated</param>
        /// <returns>Task</returns>
        private async Task UpdateSensorAsync(Sensor sensor,
            IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                @"UPDATE devices_on_pins
                    SET
                        pin_name = @PinName,
                        status = @PinStatus::STATUS,
                        device_name = @DeviceName,
                        description = @Description,
                        measurement_type = @MeasurementType,
                        sensor_type = @SensorType::SENSOR_TYPE,
                        coefficient_a = @CoefficientA,
                        coefficient_b = @CoefficientB
                    WHERE id = @Id;",
                new
                {
                    sensor.Id,
                    sensor.PinName,
                    PinStatus = sensor.PinStatus.ToString().ToLower(),
                    DeviceName = sensor.Name,
                    sensor.Description,
                    sensor.MeasurementType,
                    SensorType = sensor.Type.ToString().ToLower(),
                    sensor.CoefficientA,
                    sensor.CoefficientB
                });

        /// <summary>
        ///   Deletes Device using specified transaction
        /// </summary>
        /// <param name="id">Device Id</param>
        /// <param name="transaction">Transaction in which entity is to be deleted</param>
        /// <returns>Task</returns>
        private async Task DeleteDeviceAsync(Guid id, IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                "DELETE FROM devices_on_pins WHERE id = @Id",
                new
                {
                    Id = id
                });

        /// <summary>
        ///   Adds Raspberry without its devices using specified transaction
        /// </summary>
        /// <param name="raspberry">Raspberry to be added</param>
        /// <param name="transaction">Transaction in which entity is to be added</param>
        /// <returns>Task</returns>
        private async Task AddRaspberryAsync(Raspberry raspberry, IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                "INSERT INTO raspberries(id, name, tube) VALUES (@Id, @Name, @Tube);",
                raspberry);

        /// <summary>
        ///   Updates Raspberry without its devices using specified transaction
        /// </summary>
        /// <param name="raspberry">Raspberry to be updated</param>
        /// <param name="transaction">Transaction in which entity is to be updated</param>
        /// <returns>Task</returns>
        private async Task UpdateRaspberryAsync(Raspberry raspberry, IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                @"UPDATE raspberries
                    SET
                        name = @Name,
                        tube = @Tube
                    WHERE id = @Id;",
                raspberry);

        /// <summary>
        ///   Deletes Raspberry without explicitly deleting its devices using specified transaction. Devices might be deleted based on database configuration
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <param name="transaction">Transaction in which entity is to deleted</param>
        /// <returns>Task</returns>
        private async Task DeleteRaspberryAsync(Guid id, IDbTransaction transaction)
            => await transaction.ExecuteAsync(
                "DELETE FROM raspberries WHERE id = @Id",
                new
                {
                    Id = id
                });

        /// <summary>
        ///   Adds Raspberry without its devices
        /// </summary>
        /// <param name="raspberry">Raspberry entity to be added</param>
        /// <returns>Task</returns>
        public async Task AddRaspberryAsync(Raspberry raspberry)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    raspberry,
                    DefaultTransactionExceptionHandler,
                    AddRaspberryAsync);

        /// <summary>
        ///   Retrieves all Raspberries
        /// </summary>
        /// <returns>Task with IEnumerable of Raspberries</returns>
        public async Task<IEnumerable<Raspberry>> GetRaspberriesAsync()
            => await _sqlConnectionFactory
                .UsingConnectionAsync(
                    DefaultExceptionHandler,
                    async connection => new List<Raspberry>(await connection.QueryAsync<RaspberryDbModel>("SELECT * FROM raspberries")));

        /// <summary>
        ///   Retrieves Raspberry without its devices by Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with Raspberry without its devices</returns>
        public async Task<Raspberry> GetRaspberryAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingConnectionAsync(
                    id,
                    DefaultExceptionHandler,
                    async (guid, connection) => await connection.QuerySingleOrDefaultAsync<RaspberryDbModel>("SELECT * FROM raspberries WHERE id = @Id;",
                        new
                        {
                            Id = id
                        }));

        /// <summary>
        ///   Retrieves Raspberry with its devices by Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with Raspberry and its devices</returns>
        public async Task<Raspberry> GetRaspberryWithDevicesAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingConnectionAsync<Guid, Raspberry>(
                    id,
                    DefaultExceptionHandler,
                    async (guid, connection) =>
                    {
                        var models = (await connection.QueryAsync<RaspberryDbModel, DeviceDbModel, RaspberryDbModel>(
                            "SELECT * FROM raspberries r LEFT JOIN devices_on_pins d ON r.id = d.rpi_id WHERE r.id = @Id;",
                            (model, deviceDbModel) =>
                            {
                                if (deviceDbModel?.DeviceType == DeviceTypes.Controller)
                                {
                                    model.AddController((DeviceController)deviceDbModel?.CreateDerivedDevice());
                                }

                                if (deviceDbModel?.DeviceType == DeviceTypes.Sensor)
                                {
                                    model.AddSensor((Sensor)deviceDbModel?.CreateDerivedDevice());
                                }

                                return model;
                            },
                            new
                            {
                                Id = id
                            }, splitOn: "id, id")).ToList();

                        return !models.Any() ? null : models.Aggregate((model, dbModel) =>
                        {
                            foreach (var dbModelSensor in dbModel.Sensors)
                            {
                                model.AddSensor(dbModelSensor);
                            }

                            foreach (var dbModelController in dbModel.Controllers)
                            {
                                model.AddController(dbModelController);
                            }

                            return model;
                        });
                    });

        /// <summary>
        ///   Updates Raspberry
        /// </summary>
        /// <param name="raspberry">Raspberry to be updated</param>
        /// <returns>Task</returns>
        public async Task UpdateRaspberryAsync(Raspberry raspberry)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    raspberry,
                    DefaultTransactionExceptionHandler,
                    UpdateRaspberryAsync);

        /// <summary>
        ///   Deletes Raspberry
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task</returns>
        public async Task DeleteRaspberryAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    id,
                    DefaultTransactionExceptionHandler,
                    DeleteRaspberryAsync);

        /// <summary>
        ///   Adds DeviceController
        /// </summary>
        /// <param name="deviceController">DeviceController to be added</param>
        /// <returns>Task</returns>
        public async Task AddDeviceControllerAsync(DeviceController deviceController)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    deviceController,
                    DefaultTransactionExceptionHandler,
                    AddDeviceControllerAsync);

        /// <summary>
        ///   Retrieves DeviceControllers of Raspberry with specified Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with IEnumerable of Raspberry's DeviceControllers</returns>
        public async Task<IEnumerable<DeviceController>> GetRaspberryDeviceControllersAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingConnectionAsync(
                    id,
                    DefaultExceptionHandler,
                    async (guid, connection) => new List<DeviceController>(
                        await connection.QueryAsync<DeviceControllerDbModel>(
                            "SELECT * FROM devices_on_pins WHERE rpi_id = @RaspberryId AND device_type = @DeviceType::DEVICE_TYPE",
                            new
                            {
                                RaspberryId = guid,
                                DeviceController.DeviceType
                            })));

        /// <summary>
        ///   Retrieves DeviceController by its Id
        /// </summary>
        /// <param name="id">DeviceController Id</param>
        /// <returns>Task with DeviceController</returns>
        public async Task<DeviceController> GetDeviceControllerAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingConnectionAsync(
                    id,
                    DefaultExceptionHandler,
                    async (guid, connection) => await connection.QuerySingleOrDefaultAsync<DeviceControllerDbModel>(
                        "SELECT * FROM devices_on_pins WHERE id = @Id and device_type = @DeviceType::DEVICE_TYPE",
                        new
                        {
                            Id = id,
                            DeviceController.DeviceType
                        }));

        /// <summary>
        ///   Updates DeviceController
        /// </summary>
        /// <param name="deviceController">DeviceController to be updated</param>
        /// <returns>Task</returns>
        public async Task UpdateDeviceControllerAsync(DeviceController deviceController)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    deviceController,
                    DefaultTransactionExceptionHandler,
                    UpdateDeviceControllerAsync);

        /// <summary>
        ///   Adds Sensor
        /// </summary>
        /// <param name="sensor">Sensor to be added</param>
        /// <returns>Task</returns>
        public async Task AddSensorAsync(Sensor sensor)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    sensor,
                    DefaultTransactionExceptionHandler,
                    AddSensorAsync);

        /// <summary>
        ///   Retrieves Sensors of Raspberry with specified Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with IEnumerable of Sensors</returns>
        public async Task<IEnumerable<Sensor>> GetRaspberrySensorsAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingConnectionAsync(
                    id,
                    DefaultExceptionHandler,
                    async (guid, connection) => new List<Sensor>(
                        await connection.QueryAsync<SensorDbModel>(
                            "SELECT * FROM devices_on_pins WHERE rpi_id = @RaspberryId AND device_type = @DeviceType::DEVICE_TYPE",
                            new
                            {
                                RaspberryId = guid,
                                Sensor.DeviceType
                            })));

        /// <summary>
        ///   Retrieves Sensor by its Id
        /// </summary>
        /// <param name="id">Sensor id</param>
        /// <returns>Task with Sensor</returns>
        public async Task<Sensor> GetSensorAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingConnectionAsync(
                    id,
                    DefaultExceptionHandler,
                    async (guid, connection) => await connection.QuerySingleOrDefaultAsync<SensorDbModel>(
                        "SELECT * FROM devices_on_pins WHERE id = @Id and device_type = @DeviceType::DEVICE_TYPE;",
                        new
                        {
                            Id = id,
                            Sensor.DeviceType
                        }));

        /// <summary>
        ///   Updates Sensor
        /// </summary>
        /// <param name="sensor">Sensor to be updated</param>
        /// <returns>Task</returns>
        public async Task UpdateSensorAsync(Sensor sensor)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    sensor,
                    DefaultTransactionExceptionHandler,
                    UpdateSensorAsync);

        /// <summary>
        ///   Deletes Device (DeviceController or Sensor)
        /// </summary>
        /// <param name="id">Device Id</param>
        /// <returns>Task</returns>
        public async Task DeleteDeviceAsync(Guid id)
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    id,
                    DefaultTransactionExceptionHandler,
                    DeleteDeviceAsync);

        /// <summary>
        ///   Clears both tables
        /// </summary>
        /// <returns>Task</returns>
        public async Task ClearDatabase()
            => await _sqlConnectionFactory
                .UsingTransactionAsync(
                    DefaultTransactionExceptionHandler,
                    async transaction =>
                    {
                        await transaction.ExecuteAsync("DELETE FROM raspberries;");
                        await transaction.ExecuteAsync("DELETE FROM devices_on_pins;");
                    }
                );
    }
}