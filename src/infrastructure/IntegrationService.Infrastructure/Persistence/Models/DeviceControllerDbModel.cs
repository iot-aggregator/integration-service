﻿using System;
using IntegrationService.Application.Common;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Extensions;

namespace IntegrationService.Infrastructure.Persistence.Models
{
    internal class DeviceControllerDbModel : DeviceController
    {
        //public DeviceControllerDbModel(Guid id, Guid rpi_id, string pin_name, string device_name, string description,
        //    string status, int waiting_time_sec, int action_time_sec) : base(id, rpi_id, pin_name,
        //    device_name, description, status.ParseToPinStatus(), TimeSpan.FromSeconds(waiting_time_sec), TimeSpan.FromSeconds(action_time_sec))
        public DeviceControllerDbModel(System.Guid id, System.Guid rpi_id, System.String pin_name, System.String status,
            System.String device_type, System.String device_name, System.String description,
            System.String measurement_type, System.String sensor_type, System.Double coefficient_a,
            System.Double coefficient_b, System.Int32 waiting_time_sec, System.Int32 action_time_sec) : base(id, rpi_id,
            pin_name,
            device_name, description, status.ParseOrFail<PinStatus>(), TimeSpan.FromSeconds(waiting_time_sec),
            TimeSpan.FromSeconds(action_time_sec))
        {
        }
    }
}