﻿using System;
using IntegrationService.Application.Common;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Extensions;

namespace IntegrationService.Infrastructure.Persistence.Models
{
    public class SensorDbModel : Sensor
    {
        //public SensorDbModel(Guid id, Guid rpi_id, string pin_name, string device_name, string description,
        //    string status, string measurement_type, string sensor_type, double coefficient_a, double coefficient_b) : base(
        //    id, rpi_id, pin_name, device_name, description, status.ParseToPinStatus(), measurement_type, sensor_type.ParseToSensorType(),
        //    coefficient_a, coefficient_b)
        public SensorDbModel(System.Guid id, System.Guid rpi_id, System.String pin_name, System.String status,
            System.String device_type, System.String device_name, System.String description,
            System.String measurement_type, System.String sensor_type, System.Double coefficient_a,
            System.Double coefficient_b, System.Int32 waiting_time_sec, System.Int32 action_time_sec) : base(
            id, rpi_id, pin_name, device_name, description, status.ParseOrFail<PinStatus>(), measurement_type,
            sensor_type.ParseOrFail<SensorType>(),
            coefficient_a, coefficient_b)
        {
        }
    }
}