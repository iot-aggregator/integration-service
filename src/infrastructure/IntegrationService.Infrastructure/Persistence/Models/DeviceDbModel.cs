﻿using System;
using App.Metrics.AspNetCore;
using AutoMapper.Configuration;
using IntegrationService.Application.Common;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Extensions;

namespace IntegrationService.Infrastructure.Persistence.Models
{
    public class DeviceDbModel
    {
        private readonly Guid _id;
        private readonly Guid _rpiId;
        private readonly string _pinName;
        private readonly string _deviceName;
        private readonly string _description;
        private readonly string _status;
        private readonly int _waitingTimeSec;
        private readonly int _actionTimeSec;
        private readonly string _measurementType;
        private readonly string _sensorType;
        private readonly double _coefficientA;
        private readonly double _coefficientB;
        public string DeviceType { get; }

        public Device CreateDerivedDevice()
        {
            if (DeviceType == DeviceTypes.Sensor)
            {
                return new Sensor(_id, _rpiId, _pinName, _deviceName, _description, _status.ParseOrFail<PinStatus>(),
                    _measurementType, _sensorType.ParseOrFail<SensorType>(), _coefficientA, _coefficientB);
            }

            if (DeviceType == DeviceTypes.Controller)
            {
                return new DeviceController(_id, _rpiId, _pinName, _deviceName, _description,
                    _status.ParseOrFail<PinStatus>(), TimeSpan.FromSeconds(_waitingTimeSec),
                    TimeSpan.FromSeconds(_actionTimeSec));
            }

            throw new ArgumentOutOfRangeException(
                $"Device type {DeviceType} is not handled in {nameof(CreateDerivedDevice)} metod.");
        }

        //public static Device CreateDerivedDevice(Guid id, Guid rpiId, string pinName, string deviceName,
        //    string description, string status, int waitingTimeSec, int actionTimeSec, string measurementType,
        //    string sensorType, double coefficientA, double coefficientB, string deviceType)
        //{
        //    if (deviceType == DeviceTypes.Sensor)
        //    {
        //        return new SensorDbModel(id, rpiId, pinName, deviceName, description, status, measurementType,
        //            sensorType, coefficientA, coefficientB);
        //    }

        //    if (deviceType == DeviceTypes.Controller)
        //    {
        //        return new DeviceControllerDbModel(id, rpiId, pinName, deviceName, description, status,
        //            waitingTimeSec, actionTimeSec);
        //    }

        //    throw new ArgumentOutOfRangeException(
        //        $"Device type {deviceType} is not handled in {nameof(CreateDerivedDevice)} metod.");
        //}

        //public DeviceDbModel(Guid id, Guid rpi_id, string pin_name, string device_name,
        //    string description, string status, int waiting_time_sec, int action_time_sec, string measurement_type,
        //    string sensor_type, double coefficient_a, double coefficient_b, string device_type)
        public DeviceDbModel(System.Guid id, System.Guid rpi_id, System.String pin_name, System.String status,
            System.String device_type, System.String device_name, System.String description,
            System.String measurement_type, System.String sensor_type, System.Double coefficient_a,
            System.Double coefficient_b, System.Int32 waiting_time_sec, System.Int32 action_time_sec)
        {
            _id = id;
            _rpiId = rpi_id;
            _pinName = pin_name;
            _deviceName = device_name;
            _description = description;
            _status = status;
            _waitingTimeSec = waiting_time_sec;
            _actionTimeSec = action_time_sec;
            _measurementType = measurement_type;
            _sensorType = sensor_type;
            _coefficientA = coefficient_a;
            _coefficientB = coefficient_b;
            DeviceType = device_type;
        }
    }
}