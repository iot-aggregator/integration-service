﻿using System;
using System.Collections.Generic;
using System.Linq;
using IntegrationService.Domain.Entities;

namespace IntegrationService.Infrastructure.Persistence.Models
{
    internal class RaspberryDbModel : Raspberry
    {
        public RaspberryDbModel(Guid id, string name, int tube) : base(id, name, tube, null, null)
        {
        }

        public RaspberryDbModel(Guid id, string name, int tube, List<Device> devices) : base(id, name, tube,
            devices.Where(x => x.GetType() == typeof(DeviceController)).Select(x => (DeviceController)x).ToList(),
            devices.Where(x => x.GetType() == typeof(Sensor)).Select(x => (Sensor)x).ToList())
        {
        }
    }
}