﻿using System.Data.Common;
using System.Threading.Tasks;
using Npgsql;

namespace IntegrationService.Infrastructure.Persistence
{
    public interface ISqlConnectionFactory
    {
        /// <summary>
        ///   Creates and opens connection to database
        /// </summary>
        /// <returns>Opened DbConnection</returns>
        Task<DbConnection> CreateOpenedConnectionAsync();
    }
}