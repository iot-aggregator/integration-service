﻿using System.Data.Common;
using System.Threading.Tasks;
using IntegrationService.Infrastructure.Configuration;
using MassTransit.Configuration;
using Microsoft.Extensions.Options;
using Npgsql;

namespace IntegrationService.Infrastructure.Persistence
{
    public class SqlConnectionFactory : ISqlConnectionFactory
    {
        private readonly SqlOptions _options;

        public SqlConnectionFactory(IOptions<SqlOptions> options)
        {
            _options = options.Value;
        }

        /// <summary>
        ///   Creates and opens connection to database
        /// </summary>
        /// <returns>Opened DbConnection</returns>
        public async Task<DbConnection> CreateOpenedConnectionAsync()
        {
            var conn = new NpgsqlConnection(_options.ConnectionString);

            await conn.OpenAsync();

            return conn;
        }
    }
}