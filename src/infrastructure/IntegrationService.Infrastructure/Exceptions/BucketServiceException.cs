﻿using System;

namespace IntegrationService.Infrastructure.Exceptions
{
    public class BucketServiceException : Exception
    {
        public BucketServiceException(string message) : base(message)
        {
        }
    }
}