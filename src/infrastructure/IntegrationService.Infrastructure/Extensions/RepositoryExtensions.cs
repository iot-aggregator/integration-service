﻿using System;
using System.Data;
using System.Data.Common;
using System.Threading.Tasks;
using IntegrationService.Infrastructure.Persistence;

namespace IntegrationService.Infrastructure.Extensions
{
    internal static class RepositoryExtensions
    {
        /// <summary>
        ///   Extension method that uses transaction for connection provided by <param name="sqlConnectionFactory"></param> to process <paramref name="func"/> and commit transaction
        /// </summary>
        /// <typeparam name="T">Type that is passed to <paramref name="func"/></typeparam>
        /// <param name="sqlConnectionFactory">Factory that provides connection to database</param>
        /// <param name="entity">Entity to be passed inside <paramref name="func"/></param>
        /// <param name="dbExceptionHandler">Func that handles DbException if it is thrown inside <paramref name="func"/></param>
        /// <param name="func">Func that processes data using transaction</param>
        /// <returns>Task</returns>
        internal static async Task UsingTransactionAsync<T>(
            this ISqlConnectionFactory sqlConnectionFactory,
            T entity,
            Func<DbException, DbTransaction, Task> dbExceptionHandler,
            Func<T, DbTransaction, Task> func)
        {
            await using var connection = await sqlConnectionFactory.CreateOpenedConnectionAsync();
            await using var transaction = await connection.BeginTransactionAsync();
            try
            {
                await func(entity, transaction);
                await transaction.CommitAsync();
            }
            catch (DbException e)
            {
                await dbExceptionHandler(e, transaction);
            }
        }

        /// <summary>
        ///   Extension method that uses transaction for connection provided by <param name="sqlConnectionFactory"></param> to process <paramref name="func"/> and commit transaction
        /// </summary>
        /// <param name="sqlConnectionFactory">Factory that provides connection to database</param>
        /// <param name="dbExceptionHandler">Func that handles DbException if it is thrown</param>
        /// <param name="func">Func that processes data using transaction</param>
        /// <returns></returns>
        internal static async Task UsingTransactionAsync(
            this ISqlConnectionFactory sqlConnectionFactory,
            Func<DbException, DbTransaction, Task> dbExceptionHandler,
            Func<DbTransaction, Task> func)
        {
            await using var connection = await sqlConnectionFactory.CreateOpenedConnectionAsync();
            await using var transaction = await connection.BeginTransactionAsync();
            try
            {
                await func(transaction);
                await transaction.CommitAsync();
            }
            catch (DbException e)
            {
                await dbExceptionHandler(e, transaction);
            }
        }

        /// <summary>
        ///   Extensions method that uses connection provided by <paramref name="sqlConnectionFactory"/> to retrieve data using <paramref name="func"/>
        /// </summary>
        /// <typeparam name="TIn">Type that is passed to <paramref name="func"/></typeparam>
        /// <typeparam name="TOut">Type that is returned by <paramref name="func"/></typeparam>
        /// <param name="sqlConnectionFactory">Factory that provides connection to database</param>
        /// <param name="entity">Entity to be passed inside <paramref name="func"/></param>
        /// <param name="dbExceptionHandler">Func that handles DbException if it is thrown inside <paramref name="func"/></param>
        /// <param name="func">Func that retrieves data from database</param>
        /// <returns></returns>
        internal static async Task<TOut> UsingConnectionAsync<TIn, TOut>(
            this ISqlConnectionFactory sqlConnectionFactory,
            TIn entity,
            Func<DbException, Task> dbExceptionHandler,
            Func<TIn, IDbConnection, Task<TOut>> func)
        {
            await using var connection = await sqlConnectionFactory.CreateOpenedConnectionAsync();
            try
            {
                return await func(entity, connection);
            }
            catch (DbException e)
            {
                await dbExceptionHandler(e);
                return default;
            }
        }

        /// <summary>
        ///   Extensions method that uses connection provided by <paramref name="sqlConnectionFactory"/> to retrieve data using <paramref name="func"/>
        /// </summary>
        /// <typeparam name="TOut">Type that is returned by <paramref name="func"/></typeparam>
        /// <param name="sqlConnectionFactory">Factory that provides connection to database</param>
        /// <param name="dbExceptionHandler">Func that handles DbException if it is thrown inside <paramref name="func"/></param>
        /// <param name="func">Func that retrieves data from database</param>
        /// <returns></returns>
        internal static async Task<TOut> UsingConnectionAsync<TOut>(
            this ISqlConnectionFactory sqlConnectionFactory,
            Func<DbException, Task> dbExceptionHandler,
            Func<IDbConnection, Task<TOut>> func)
        {
            await using var connection = await sqlConnectionFactory.CreateOpenedConnectionAsync();
            try
            {
                return await func(connection);
            }
            catch (DbException e)
            {
                await dbExceptionHandler(e);
                return default;
            }
        }
    }
}