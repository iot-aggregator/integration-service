﻿using System;
using System.Collections.Generic;

namespace IntegrationService.Infrastructure.Events
{
    public interface IPinMeasured
    {
        public Guid InstanceId { get; set; }
        public DateTimeOffset MeasuredAt { get; set; }
        public string PinName { get; set; }
        public double MeasuredValue { get; set; }
        public Dictionary<string, string> Tags { get; set; }
    }
}