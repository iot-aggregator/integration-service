﻿using System;
using System.Linq;
using System.Threading.Tasks;
using App.Metrics;
using App.Metrics.Gauge;
using IntegrationService.Application.Services;

namespace IntegrationService.Infrastructure.Services
{
    public class MeasurementPublisher : IMeasurementPublisher
    {
        private readonly IMetrics _metrics;

        public MeasurementPublisher(IMetrics metrics)
        {
            _metrics = metrics;
        }

        public Task PublishGaugeMeasurementAsync(
            string context,
            string name,
            string unit,
            double value,
            params (string key, string value)[] tags)
        {
            _ = context switch
            {
                null => throw new ArgumentNullException(nameof(context)),
                var x when string.IsNullOrWhiteSpace(x) => throw new ArgumentException(
                    "Context can not be empty or whitespace.", nameof(context)),
                _ => 0
            };

            _ = name switch
            {
                null => throw new ArgumentNullException(nameof(name)),
                var x when string.IsNullOrWhiteSpace(x) => throw new ArgumentException(
                    "Name can not be empty or whitespace.", nameof(name)),
                _ => 0
            };

            _ = unit switch
            {
                null => throw new ArgumentNullException(nameof(unit)),
                _ => 0
            };

            var options = new GaugeOptions
            {
                Context = context,
                Name = name,
                MeasurementUnit = Unit.Custom(unit),
                Tags = new MetricTags
                (
                    tags.Select(x => x.key).ToArray(),
                    tags.Select(x => x.value).ToArray()
                )
            };

            _metrics.Measure.Gauge.SetValue(options, value);

            return Task.CompletedTask;
        }
    }
}