﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Util;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Services;
using IntegrationService.Infrastructure.Configuration;
using IntegrationService.Infrastructure.Exceptions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace IntegrationService.Infrastructure.Services
{
    public class BucketService : IBucketService
    {
        private readonly S3PolicyOptions _policy;
        private readonly IMemoryCache _memoryCache;
        private readonly HttpClient _httpClient;
        private readonly AmazonS3Client _amazonS3Client;
        private readonly S3Options _options;
        private const string ExistingBucketsKey = "existing_buckets";

        public BucketService(IOptions<S3Options> options, IOptions<S3PolicyOptions> policy, IMemoryCache memoryCache, HttpClient client)
        {
            _options = options.Value;
            _policy = policy.Value;
            _memoryCache = memoryCache;
            _httpClient = client;

            var config = new AmazonS3Config
            {
                RegionEndpoint = RegionEndpoint.GetBySystemName(_options.Region),
                ServiceURL = _options.Host,
                ForcePathStyle = true
            };

            _amazonS3Client = new AmazonS3Client(_options.AccessKey, _options.Secret, config);
        }

        private HashSet<string> GetExistingOrNewSet()
        {
            if (_memoryCache.TryGetValue(ExistingBucketsKey, out HashSet<string> existingBuckets))
            {
                return existingBuckets;
            }

            existingBuckets = new HashSet<string>();
            _memoryCache.Set(ExistingBucketsKey, existingBuckets);

            return existingBuckets;
        }

        private async Task<HashSet<string>> UpdateCacheAsync()
        {
            var buckets = await _amazonS3Client.ListBucketsAsync();
            var newExistingBuckets = buckets.Buckets.Select(x => x.BucketName.ToLowerInvariant()).ToHashSet();
            _memoryCache.Set(ExistingBucketsKey, newExistingBuckets);

            return newExistingBuckets;
        }

        private void ThrowBucketServiceExceptionIfBucketNameNotInCollection(HashSet<string> collection, string bucketName)
        {
            if (!collection.Contains(bucketName))
            {
                throw new BucketServiceException("Bucket name does not exist");
            }
        }

        public async Task<BucketObjectsListDto> GetPagedObjectsFromBucketAsync(string bucketName, int size,
            string prefix = "",
            string listAfterMarker = "", string continuationToken = "")
        {
            _ = bucketName switch
            {
                null => throw new ArgumentNullException(nameof(bucketName)),
                var x when string.IsNullOrWhiteSpace(x) => throw new BucketServiceException(
                    "Bucket name can not be empty or whitespace"),
                _ => 0
            };

            _ = size switch
            {
                <= 0 => throw new BucketServiceException("Size can not be less or equal to 0"),
                _ => 0
            };

            _ = prefix switch
            {
                null => throw new ArgumentNullException(nameof(prefix)),
                _ => 0
            };

            _ = listAfterMarker switch
            {
                null => throw new ArgumentNullException(nameof(listAfterMarker)),
                _ => 0
            };

            _ = continuationToken switch
            {
                null => throw new ArgumentNullException(nameof(continuationToken)),
                _ => 0
            };

            var existingBuckets = GetExistingOrNewSet();
            if (!existingBuckets.Contains(bucketName))
            {
                var newExistingBuckets = await UpdateCacheAsync();
                ThrowBucketServiceExceptionIfBucketNameNotInCollection(newExistingBuckets, bucketName);
            }

            var req = new ListObjectsV2Request
            {
                BucketName = bucketName,
                MaxKeys = size
            };

            if (!string.IsNullOrWhiteSpace(listAfterMarker))
            {
                req.StartAfter = listAfterMarker;
            }

            if (!string.IsNullOrWhiteSpace(prefix))
            {
                req.Prefix = prefix;
            }

            if (!string.IsNullOrWhiteSpace(continuationToken))
            {
                req.ContinuationToken = continuationToken;
            }

            var response = await _amazonS3Client.ListObjectsV2Async(req);

            return new BucketObjectsListDto
            {
                ContinuationToken = response.NextContinuationToken,
                Links = response.S3Objects.Select(x => $"{_options.ExternalAddress}/{bucketName}/{x.Key}")
            };
        }

        public Task<Stream> GetImage(string bucketName, string path)
        {
            return _httpClient.GetStreamAsync($"{_options.Host}/{bucketName}/{path}");
        }

        public async Task UploadImage(string bucketName, string key, Stream data)
        {
            await _amazonS3Client.PutObjectAsync(new PutObjectRequest
            {
                BucketName = bucketName,
                InputStream = data,
                Key = key
            });
        }

        public async Task EnsureBucketCreated()
        {
            var doesBucketExist = await AmazonS3Util.DoesS3BucketExistV2Async(_amazonS3Client, _options.BucketName);
            if (!doesBucketExist)
            {
                await _amazonS3Client.PutBucketAsync(new PutBucketRequest
                {
                    BucketName = _options.BucketName,
                    UseClientRegion = true
                });
            }

            var policy = await _amazonS3Client.GetBucketPolicyAsync(_options.BucketName);
            var policyDeserialized = JsonSerializer.Deserialize<S3PolicyOptions>(policy?.Policy ?? "{}");
            if (policy?.Policy != null)
            {
                var retrieved = JsonSerializer.Serialize(policyDeserialized);
                var created = JsonSerializer.Serialize(_policy);
                if (retrieved == created)
                {
                    return;
                }
            }

            await _amazonS3Client.PutBucketPolicyAsync(new PutBucketPolicyRequest
            {
                BucketName = _options.BucketName,
                Policy = JsonSerializer.Serialize(_policy)
            });
        }
    }
}