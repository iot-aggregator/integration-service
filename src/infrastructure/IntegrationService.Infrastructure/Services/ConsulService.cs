﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Consul;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Services;
using IntegrationService.Infrastructure.Configuration;
using Microsoft.Extensions.Options;

namespace IntegrationService.Infrastructure.Services
{
    public class ConsulService : IConsulService
    {
        private readonly IConsulClient _client;
        private readonly IMapper _mapper;
        private readonly ConsulOptions _options;

        public ConsulService(IOptions<ConsulOptions> options, IConsulClient client, IMapper mapper)
        {
            _options = options.Value;
            _client = client;
            _mapper = mapper;
        }

        public async Task RegisterServiceAsync(string id, string name, string host, int port,
            CancellationToken cancellationToken = default, params HealthCheckDto[] healthChecks)
        {
            var registration = new AgentServiceRegistration
            {
                ID = id,
                Name = _options.ServiceName,
                Address = _options.ServiceAddress.Host,
                Port = _options.ServiceAddress.Port,
                Checks = _mapper.Map<AgentServiceCheck[]>(healthChecks)
            };

            await _client.Agent.ServiceDeregister(registration.ID, cancellationToken);
            await _client.Agent.ServiceRegister(registration, cancellationToken);
        }

        public async Task DeregisterServiceAsync(string id, CancellationToken cancellationToken = default)
        {
            await _client.Agent.ServiceDeregister(id, cancellationToken);
        }
    }
}