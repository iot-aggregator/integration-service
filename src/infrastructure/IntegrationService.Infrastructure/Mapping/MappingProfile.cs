﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using Consul;
using IntegrationService.Application.Common;
using IntegrationService.Application.DeviceControllers.Commands.CreateDeviceController;
using IntegrationService.Application.DeviceControllers.Commands.UpdateDeviceController;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Raspberries.Commands.CreateRaspberry;
using IntegrationService.Application.Raspberries.Commands.UpdateRaspberry;
using IntegrationService.Application.Sensors.Commands.CreateSensor;
using IntegrationService.Application.Sensors.Commands.UpdateSensor;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Extensions;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationService.Infrastructure.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<HealthCheckDto, AgentServiceCheck>()
                .ForMember(x => x.Name,
                    x =>
                        x.MapFrom(y => y.Name))
                .ForMember(x => x.HTTP,
                    x =>
                        x.MapFrom(y => y.Address))
                .ForMember(x => x.Method,
                    x =>
                        x.MapFrom(y => y.Method))
                .ForMember(x => x.Interval,
                    x =>
                        x.MapFrom(y => y.Interval))
                .ForMember(x => x.Timeout,
                    x =>
                        x.MapFrom(y => y.Timeout))
                .ForMember(x => x.TLSSkipVerify,
                    x =>
                        x.MapFrom(y => true))
                .ForMember(x => x.Header,
                    x =>
                        x.MapFrom(y =>
                            new Dictionary<string, List<string>>
                            {
                                {
                                    "Content-Type",
                                    new List<string>
                                    {
                                        "application/json"
                                    }
                                }
                            }));

            CreateMap<CreateRaspberryCommand, Raspberry>()
                .ForCtorParam("id",
                    x =>
                        x.MapFrom(y => Guid.NewGuid()))
                .ForCtorParam("name",
                    x =>
                        x.MapFrom(y => y.Name))
                .ForCtorParam("tube",
                    x =>
                        x.MapFrom(y => y.Tube))
                .ForCtorParam("controllers",
                    x =>
                        x.MapFrom(y => new List<Controller>()))
                .ForCtorParam("sensors",
                    x =>
                        x.MapFrom(y => new List<Sensor>()));

            CreateMap<UpdateRaspberryCommand, Raspberry>()
                .ForCtorParam("id",
                    x =>
                        x.MapFrom(y => y.RpiId))
                .ForCtorParam("name",
                    x =>
                        x.MapFrom(y => y.Name))
                .ForCtorParam("tube",
                    x =>
                        x.MapFrom(y => y.Tube))
                .ForCtorParam("controllers",
                    x =>
                        x.MapFrom(y => new List<Controller>()))
                .ForCtorParam("sensors",
                    x =>
                        x.MapFrom(y => new List<Sensor>()));

            CreateMap<Raspberry, RaspberryBlockDto>()
                .ForMember(x => x.Id,
                    x =>
                        x.MapFrom(y => y.Id))
                .ForMember(x => x.Name,
                    x =>
                        x.MapFrom(y => y.Name))
                .ForMember(x => x.Tube,
                    x =>
                        x.MapFrom(y => y.Tube));

            CreateMap<Raspberry, RaspberryConfigurationDto>()
                .ForMember(x => x.Controllers,
                    x =>
                        x.MapFrom((raspberry, dto, _, ctx) =>
                            ctx.Mapper.Map<List<DeviceControllerDto>>(raspberry.Controllers)))
                .ForMember(x => x.Sensors,
                    x =>
                        x.MapFrom((raspberry, dto, _, ctx) =>
                            ctx.Mapper.Map<List<SensorDto>>(raspberry.Sensors)));

            CreateMap<Raspberry, RaspberryDto>()
                .ForMember(x => x.Id,
                    x =>
                        x.MapFrom(y => y.Id))
                .ForMember(x => x.Name,
                    x =>
                        x.MapFrom(y => y.Name))
                .ForMember(x => x.Tube,
                    x =>
                        x.MapFrom(y => y.Tube))
                .ForMember(x => x.Config,
                    x =>
                        x.MapFrom((raspberry, dto, _, ctx) => ctx.Mapper.Map<RaspberryConfigurationDto>(raspberry)));

            CreateMap<CreateDeviceControllerCommand, DeviceController>()
                .ForCtorParam("id",
                    x =>
                        x.MapFrom(y => Guid.NewGuid()))
                .ForCtorParam("raspberryId",
                    x =>
                        x.MapFrom(y => y.RpiId))
                .ForCtorParam("pinName",
                    x =>
                        x.MapFrom(y => y.PinName))
                .ForCtorParam("pinStatus",
                    x =>
                        x.MapFrom(y => y.Status.ParseOrFail<PinStatus>(true)))
                .ForCtorParam("name",
                    x =>
                        x.MapFrom(y => y.Name))
                .ForCtorParam("description",
                    x =>
                        x.MapFrom(y => y.Description))
                .ForCtorParam("waitingTime",
                    x =>
                        x.MapFrom(y => TimeSpan.FromSeconds(y.WaitingTimeInSeconds)))
                .ForCtorParam("actionTime",
                    x =>
                        x.MapFrom(y => TimeSpan.FromSeconds(y.ActionTimeInSeconds)));

            CreateMap<UpdateDeviceControllerCommand, DeviceController>()
                .ForCtorParam("id",
                    x =>
                        x.MapFrom(y => y.Id))
                .ForCtorParam("raspberryId",
                    x =>
                        x.MapFrom(y => y.RpiId))
                .ForCtorParam("pinName",
                    x =>
                        x.MapFrom(y => y.PinName))
                .ForCtorParam("pinStatus",
                    x =>
                        x.MapFrom(y => y.Status.ParseOrFail<PinStatus>(true)))
                .ForCtorParam("name",
                    x =>
                        x.MapFrom(y => y.Name))
                .ForCtorParam("description",
                    x =>
                        x.MapFrom(y => y.Description))
                .ForCtorParam("waitingTime",
                    x =>
                        x.MapFrom(y => TimeSpan.FromSeconds(y.WaitingTimeInSeconds)))
                .ForCtorParam("actionTime",
                    x =>
                        x.MapFrom(y => TimeSpan.FromSeconds(y.ActionTimeInSeconds)));

            CreateMap<DeviceController, DeviceControllerDto>()
                .ForMember(x => x.Id,
                    x =>
                        x.MapFrom(y => y.Id))
                .ForMember(x => x.PinName,
                    x =>
                        x.MapFrom(y => y.PinName))
                .ForMember(x => x.Status,
                    x =>
                        x.MapFrom(y => y.PinStatus.ToLowerInvariantString()))
                .ForMember(x => x.Name,
                    x =>
                        x.MapFrom(y => y.Name))
                .ForMember(x => x.Description,
                    x =>
                        x.MapFrom(y => y.Description))
                .ForMember(x => x.WaitingTimeInSeconds,
                    x =>
                        x.MapFrom(y => y.WaitingTime.TotalSeconds))
                .ForMember(x => x.ActionTimeInSeconds,
                    x =>
                        x.MapFrom(y => y.ActionTime.TotalSeconds));

            CreateMap<CreateSensorCommand, Sensor>()
                .ForCtorParam("id",
                    x =>
                        x.MapFrom(y => Guid.NewGuid()))
                .ForCtorParam("raspberryId",
                    x =>
                        x.MapFrom(y => y.RpiId))
                .ForCtorParam("pinName",
                    x =>
                        x.MapFrom(y => y.PinName))
                .ForCtorParam("pinStatus",
                    x =>
                        x.MapFrom(y => y.Status.ParseOrFail<PinStatus>(true)))
                .ForCtorParam("name",
                    x =>
                        x.MapFrom(y => y.Name))
                .ForCtorParam("description",
                    x =>
                        x.MapFrom(y => y.Description))
                .ForCtorParam("type",
                    x =>
                        x.MapFrom(y => y.Type.ParseOrFail<SensorType>(true)))
                .ForCtorParam("measurementType",
                    x =>
                        x.MapFrom(y => y.MeasurementType))
                .ForCtorParam("coefficientA",
                    x =>
                        x.MapFrom(y => y.CoefficientA))
                .ForCtorParam("coefficientB",
                    x =>
                        x.MapFrom(y => y.CoefficientB));

            CreateMap<UpdateSensorCommand, Sensor>()
                .ForCtorParam("id",
                    x =>
                        x.MapFrom(y => y.Id))
                .ForCtorParam("raspberryId",
                    x =>
                        x.MapFrom(y => y.RpiId))
                .ForCtorParam("pinName",
                    x =>
                        x.MapFrom(y => y.PinName))
                .ForCtorParam("pinStatus",
                    x =>
                        x.MapFrom(y => y.Status.ParseOrFail<PinStatus>(true)))
                .ForCtorParam("name",
                    x =>
                        x.MapFrom(y => y.Name))
                .ForCtorParam("description",
                    x =>
                        x.MapFrom(y => y.Description))
                .ForCtorParam("type",
                    x =>
                        x.MapFrom(y => y.Type.ParseOrFail<SensorType>(true)))
                .ForCtorParam("measurementType",
                    x =>
                        x.MapFrom(y => y.MeasurementType))
                .ForCtorParam("coefficientA",
                    x =>
                        x.MapFrom(y => y.CoefficientA))
                .ForCtorParam("coefficientB",
                    x =>
                        x.MapFrom(y => y.CoefficientB));

            CreateMap<Sensor, SensorDto>()
                .ForMember(x => x.Id,
                    x =>
                        x.MapFrom(y => y.Id))
                .ForMember(x => x.PinName,
                    x =>
                        x.MapFrom(y => y.PinName))
                .ForMember(x => x.Status,
                    x =>
                        x.MapFrom(y => y.PinStatus.ToLowerInvariantString()))
                .ForMember(x => x.Name,
                    x =>
                        x.MapFrom(y => y.Name))
                .ForMember(x => x.Description,
                    x =>
                        x.MapFrom(y => y.Description))
                .ForMember(x => x.MeasurementType,
                    x =>
                        x.MapFrom(y => y.MeasurementType))
                .ForMember(x => x.SensorType,
                    x =>
                        x.MapFrom(y => y.Type.ToLowerInvariantString()))
                .ForMember(x => x.CoefficientA,
                    x =>
                        x.MapFrom(y => y.CoefficientA))
                .ForMember(x => x.CoefficientB,
                    x =>
                        x.MapFrom(y => y.CoefficientB));
        }
    }
}