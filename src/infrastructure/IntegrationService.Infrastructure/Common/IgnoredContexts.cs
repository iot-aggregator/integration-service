﻿namespace IntegrationService.Infrastructure.Common
{
    public static class IgnoredContexts
    {
        public static string Application => "Application";
    }
}