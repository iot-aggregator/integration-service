﻿namespace IntegrationService.Infrastructure.Configuration
{
    public class SqlOptions
    {
        public string ConnectionString { get; set; }
    }
}