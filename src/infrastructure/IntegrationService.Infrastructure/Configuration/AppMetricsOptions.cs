﻿namespace IntegrationService.Infrastructure.Configuration
{
    public class AppMetricsOptions
    {
        public string[] IncludedContexts { get; set; }
        public string[] IncludedNames { get; set; }
        public string[] IncludedTypes { get; set; }
    }
}