﻿using System;

namespace IntegrationService.Infrastructure.Configuration
{
    public class RabbitMqOptions
    {
        public Uri Host { get; set; }
    }
}