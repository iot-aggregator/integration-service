﻿namespace IntegrationService.Infrastructure.Configuration
{
    public class S3Options
    {
        public string Host { get; set; }
        public string ExternalAddress { get; set; }
        public string AccessKey { get; set; }
        public string Secret { get; set; }
        public string Region { get; set; }
        public bool EnsureBucketExists { get; set; }
        public string BucketName { get; set; }
    }
}