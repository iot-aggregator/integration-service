﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Consul;
using IntegrationService.Application.Services;
using IntegrationService.Infrastructure.Configuration;
using IntegrationService.Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IntegrationService.Infrastructure.Installers
{
    public static class ConsulInstaller
    {
        /// <summary>
        ///     Retrieves options from IConfiguration.
        /// </summary>
        /// <param name="configuration">IConfiguration that contains Consul section.</param>
        /// <returns>Bound ConsulOptions.</returns>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="configuration" /> Consul section is empty.
        /// </exception>
        public static ConsulOptions GetConsulOptions(this IConfiguration configuration)
        {
            var options = new ConsulOptions();

            configuration.GetConsulSection().Bind(options);

            return options;
        }

        /// <summary>
        ///     Retrieves options section from IConfiguration.
        /// </summary>
        /// <param name="configuration">IConfiguration that contains Consul section.</param>
        /// <returns>Consul section.</returns>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="configuration" /> Consul section is empty.
        /// </exception>
        public static IConfiguration GetConsulSection(this IConfiguration configuration)
        {
            if (!configuration.GetSection("Consul").Exists())
                throw new ArgumentException(
                    "Consul settings section must be set.");

            return configuration.GetSection("Consul");
        }

        /// <summary>
        ///     Creates Consul client based on provided configuration.
        /// </summary>
        /// <param name="options">Options for Consul client.</param>
        /// <returns>Consul client.</returns>
        /// <exception cref="ArgumentNullException">
        ///     When <paramref name="options" /> is null.
        /// </exception>
        private static ConsulClient CreateConsulClient(ConsulOptions options)
        {
            if (options.ServiceDiscoveryAddress == null) throw new ArgumentNullException(nameof(options));

            return new ConsulClient(config => { config.Address = options.ServiceDiscoveryAddress; });
        }

        /// <summary>
        ///     Retrieves URI for RabbitMQ connection using Consul service discovery.
        /// </summary>
        /// <param name="options">Options for Consul client.</param>
        /// <returns>URI for RabbitMQ connection.</returns>
        /// <exception cref="ArgumentNullException">
        ///     When <paramref name="options" /> is null.
        /// </exception>
        public static async Task<Uri> GetRabbitMqHostUsingConsulAsync(this ConsulOptions options)
        {
            var consulClient = CreateConsulClient(options);

            var allServices = await consulClient.Agent.Services();
            var rabbitService = allServices.Response
                .First(s => s.Value.Service.Equals("rabbit", StringComparison.OrdinalIgnoreCase)).Value;
            return new Uri($"amqp://{rabbitService.Address}:{rabbitService.Port}");
        }

        public static async Task<string> GetMinIoHostUsingConsulAsync(this ConsulOptions options)
        {
            var consulClient = CreateConsulClient(options);
            var allServices = await consulClient.Agent.Services();
            var minIoService = allServices.Response
                .First(s => s.Value.Service.Equals("minio", StringComparison.OrdinalIgnoreCase)).Value;

            return $"http://{minIoService.Address}:{minIoService.Port}";
        }

        /// <summary>
        ///     Installs Consul in provided IServiceCollection.
        /// </summary>
        /// <param name="services">IServiceCollection selected for Consul configuration.</param>
        /// <param name="configuration">Configuration that contains Consul section.</param>
        /// <returns>The same IServiceCollection with installed Consul.</returns>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="configuration" /> Consul section is empty.
        /// </exception>
        public static IServiceCollection InstallConsul(this IServiceCollection services, IConfiguration configuration)
        {
            var options = configuration.GetConsulOptions();

            return services.InstallConsul(options);
        }

        /// <summary>
        ///     Installs Consul in provided IServiceCollection.
        /// </summary>
        /// <param name="services">IServiceCollection selected for Consul configuration.</param>
        /// <param name="options">Options for Consul client.</param>
        /// <returns>The same IServiceCollection with installed Consul.</returns>
        public static IServiceCollection InstallConsul(this IServiceCollection services, ConsulOptions options)
        {
            services.AddSingleton<IConsulClient, ConsulClient>(p => CreateConsulClient(options));
            services.AddTransient<IConsulService, ConsulService>();

            return services;
        }
    }
}