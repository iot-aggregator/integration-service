﻿using System;
using IntegrationService.Infrastructure.Configuration;
using IntegrationService.Infrastructure.Consumers;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IntegrationService.Infrastructure.Installers
{
    public static class MassTransitInstaller
    {
        /// <summary>
        ///     Retrieves options from IConfiguration.
        /// </summary>
        /// <param name="configuration">IConfiguration that contains RabbitMq section.</param>
        /// <returns>Bound RabbitMqOptions.</returns>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="configuration" /> RabbitMq section is empty.
        /// </exception>
        private static RabbitMqOptions GetRabbitMqOptions(this IConfiguration configuration)
        {
            var rabbitMqOptions = new RabbitMqOptions();

            if (!configuration.GetSection("RabbitMq").Exists())
                throw new ArgumentException(
                    "RabbitMq settings section must be set.");

            configuration.GetSection("RabbitMq").Bind(rabbitMqOptions);

            return rabbitMqOptions;
        }

        /// <summary>
        ///     Installs MassTransit with RabbitMq in provided IServiceCollection.
        /// </summary>
        /// <param name="services">IServiceCollection selected for MassTransit configuration.</param>
        /// <param name="configuration">Configuration that contains RabbitMq section.</param>
        /// <returns>The same IServiceCollection with installed MassTransit.</returns>
        public static IServiceCollection InstallMassTransit(this IServiceCollection services,
            IConfiguration configuration)
        {
            var rabbitOptions = configuration.GetRabbitMqOptions();

            return services.InstallMassTransit(rabbitOptions.Host);
        }

        /// <summary>
        ///     Installs MassTransit with RabbitMq in provided IServiceCollection.
        /// </summary>
        /// <param name="services">IServiceCollection selected for MassTransit configuration.</param>
        /// <param name="rabbitMqHost">URI for RabbitMQ connection.</param>
        /// <returns>The same IServiceCollection with installed MassTransit.</returns>
        public static IServiceCollection InstallMassTransit(this IServiceCollection services,
            Uri rabbitMqHost)
        {
            services.AddMassTransit(x =>
            {
                x.AddConsumersFromNamespaceContaining<PinMeasuredConsumer>();
                x.SetKebabCaseEndpointNameFormatter();

                x.UsingRabbitMq((context, cfg) =>
                {
                    cfg.Host(rabbitMqHost);

                    cfg.ConfigureEndpoints(context);
                });
            });
            services.AddMassTransitHostedService();

            return services;
        }
    }
}