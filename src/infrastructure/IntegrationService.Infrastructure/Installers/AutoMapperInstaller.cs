﻿using IntegrationService.Infrastructure.Mapping;
using Microsoft.Extensions.DependencyInjection;

namespace IntegrationService.Infrastructure.Installers
{
    public static class AutoMapperInstaller
    {
        /// <summary>
        ///     Installs AutoMapper in provided IServiceCollection.
        /// </summary>
        /// <param name="services">IServiceCollection selected for AutoMapper configuration.</param>
        /// <returns>The same IServiceCollection with installed AutoMapper.</returns>
        public static IServiceCollection InstallAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(MappingProfile));

            return services;
        }
    }
}