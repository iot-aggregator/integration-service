﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using Amazon.S3.Model.Internal.MarshallTransformations;
using App.Metrics;
using App.Metrics.AspNetCore;
using App.Metrics.Formatters.Prometheus;
using IntegrationService.Infrastructure.Common;
using IntegrationService.Infrastructure.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using MetricType = App.Metrics.MetricType;

namespace IntegrationService.Infrastructure.Installers
{
    public static class AppMetricsInstaller
    {
        /// <summary>
        ///     Resets global tags for all published metrics.
        /// </summary>
        /// <param name="builder">IMetricsBuilder selected for global tags reset.</param>
        /// <returns>The same IMetricsBuilder with metrics global tags reset.</returns>
        private static IMetricsBuilder ResetGlobalTags(this IMetricsBuilder builder)
        {
            return builder.Configuration.Configure(options => { options.WithGlobalTags((dictionary, info) => { }); });
        }

        /// <summary>
        ///     Retrieves options from IConfiguration.
        /// </summary>
        /// <param name="configuration">IConfiguration that contains AppMetrics section.</param>
        /// <returns>Bound AppMetricsOptions.</returns>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="configuration" /> AppMetrics section is empty.
        /// </exception>
        private static AppMetricsOptions GetAppMetricsOptions(this IConfiguration configuration)
        {
            var appMetricsOptions = new AppMetricsOptions();

            if (!configuration.GetSection("AppMetrics").Exists())
                throw new ArgumentException(
                    "AppMetrics settings section must be set.");

            configuration.GetSection("AppMetrics").Bind(appMetricsOptions);

            return appMetricsOptions;
        }

        /// <summary>
        ///     Configures AppMetrics endpoints for Prometheus.
        /// </summary>
        /// <param name="builder">IHostBuilder selected for endpoints configuration.</param>
        /// <returns>The same IHostBuilder with configured filters.</returns>
        private static IHostBuilder ConfigureAppMetricsEndpoints(this IHostBuilder builder)
        {
            return builder.UseMetrics(options =>
            {
                options.EndpointOptions = endpointOptions =>
                {
                    endpointOptions.MetricsTextEndpointOutputFormatter = new MetricsPrometheusTextOutputFormatter();
                    endpointOptions.MetricsEndpointOutputFormatter = new MetricsPrometheusProtobufOutputFormatter();
                    endpointOptions.EnvironmentInfoEndpointEnabled = false;
                };
            });
        }

        /// <summary>
        ///     Configures filters for AppMetrics.
        /// </summary>
        /// <param name="builder">IMetricsBuilder selected for filters configuration.</param>
        /// <param name="filterContexts">Contexts filter flag. True if contexts should be filtered based on provided HashSet.</param>
        /// <param name="filterNames">Names filter flag. True if names should be filtered based on provided HashSet.</param>
        /// <param name="filterTypes">Types filter flag. True if contexts should be filtered based on provided array.</param>
        /// <param name="includedContexts">HashSet with contexts that should be included in metrics filter.</param>
        /// <param name="includedNames">HashSet with names that should be included in metrics filter.</param>
        /// <param name="includedTypes">Array with types that should be included in metrics filter.</param>
        /// <returns>The same IMetricsBuilder with configured filters.</returns>
        private static IMetricsBuilder BuildFilter(this IMetricsBuilder builder, bool filterContexts, bool filterNames,
            bool filterTypes,
            ImmutableHashSet<string> includedContexts, ImmutableHashSet<string> includedNames,
            MetricType[] includedTypes)
        {
            return builder.Filter.With(metricsBuilder =>
            {
                metricsBuilder.WhereContext(ctx =>
                {
                    // Ignore ASP.NET application metrics
                    if (ctx.StartsWith(IgnoredContexts.Application))
                    {
                        return false;
                    }

                    if (filterContexts)
                    {
                        return includedContexts.Contains(ctx);
                    }

                    return true;
                });

                if (filterNames) metricsBuilder.WhereName(ctx => includedNames.Contains(ctx.Split("|").First()));

                if (filterTypes) metricsBuilder.WhereType(includedTypes);
            });
        }

        /// <summary>
        ///     Installs Prometheus endpoints using AppMetrics in provided IHostBuilder with filtered contexts, names and/or metric
        ///     types.
        /// </summary>
        /// <param name="hostBuilder">IHostBuilder that was selected to have AppMetrics installed.</param>
        /// <param name="configuration">Configuration that contains AppMetrics section.</param>
        /// <returns>The same IHostBuilder with installed AppMetrics.</returns>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="configuration" /> AppMetrics section is empty.
        /// </exception>
        public static IHostBuilder InstallAppMetrics(this IHostBuilder hostBuilder, IConfiguration configuration)
        {
            hostBuilder.ConfigureMetrics(builder =>
                {
                    var appMetricsOptions = configuration.GetAppMetricsOptions();

                    builder.ResetGlobalTags();

                    var filterContexts = !appMetricsOptions.IncludedContexts.Contains("All");
                    var filterNames = !appMetricsOptions.IncludedNames.Contains("All");
                    var filterTypes = !appMetricsOptions.IncludedTypes.Contains("All");
                    var includedTypes = new List<MetricType>();

                    if (filterTypes)
                        foreach (var includedType in appMetricsOptions.IncludedTypes)
                        {
                            if (!Enum.TryParse<MetricType>(includedType, out var parsedType))
                                throw new ArgumentException(
                                    $"Invalid metric type in {nameof(AppMetricsOptions)}.{nameof(AppMetricsOptions.IncludedTypes)}");

                            includedTypes.Add(parsedType);
                        }

                    builder.BuildFilter(
                        filterContexts,
                        filterNames,
                        filterTypes,
                        appMetricsOptions.IncludedContexts.ToImmutableHashSet(),
                        appMetricsOptions.IncludedNames.ToImmutableHashSet(),
                        includedTypes.ToArray()
                    );
                })
                .ConfigureAppMetricsEndpoints();

            return hostBuilder;
        }
    }
}