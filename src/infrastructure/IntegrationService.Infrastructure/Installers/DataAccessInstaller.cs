﻿using IntegrationService.Application.Repositories;
using IntegrationService.Infrastructure.Configuration;
using IntegrationService.Infrastructure.Persistence;
using IntegrationService.Infrastructure.Persistence.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace IntegrationService.Infrastructure.Installers
{
    public static class DataAccessInstaller
    {
        public static IServiceCollection InstallDataAccess(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<SqlOptions>(options => 
                options.ConnectionString = configuration.GetConnectionString("SqlConnectionString"));

            services.AddScoped<ISqlConnectionFactory, SqlConnectionFactory>();
            services.AddScoped<IRaspberryRepository, SqlRaspberryRepository>();

            return services;
        }
    }
}