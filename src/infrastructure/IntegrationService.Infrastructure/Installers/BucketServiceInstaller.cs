﻿using System;
using System.IO;
using System.Text.Json;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Services;
using IntegrationService.Infrastructure.Services;
using MassTransit.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace IntegrationService.Infrastructure.Installers
{
    public static class BucketServiceInstaller
    {
        /// <summary>
        ///     Retrieves options section from IConfiguration.
        /// </summary>
        /// <param name="configuration">IConfiguration that contains S3 section.</param>
        /// <returns>S3 section.</returns>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="configuration" /> S3 section is empty.
        /// </exception>
        public static IConfiguration GetS3Section(this IConfiguration configuration)
        {
            if (!configuration.GetSection("S3").Exists())
                throw new ArgumentException(
                    "S3 settings section must be set.");

            return configuration.GetSection("S3");
        }

        public static IServiceCollection ConfigureBucketPolicy(this IServiceCollection services, string bucketName)
        {
            var policySerializedTemplate = File.ReadAllText("bucket-policy.json");
            policySerializedTemplate = policySerializedTemplate.Replace("{{BucketName}}", bucketName);

            var policyOptions = JsonSerializer.Deserialize<S3PolicyOptions>(policySerializedTemplate);
            
            services.AddSingleton<IOptions<S3PolicyOptions>>(Options.Create(policyOptions));

            return services;
        }

        /// <summary>
        ///     Installs BucketService in provided IServiceCollection.
        /// </summary>
        /// <param name="services">IServiceCollection selected for BucketService configuration.</param>
        /// <param name="configuration">Configuration that contains S3 section.</param>
        /// <returns>The same IServiceCollection with installed BucketService.</returns>
        public static IServiceCollection InstallBucketService(this IServiceCollection services,
            IConfiguration configuration)
        {
            return services.AddTransient<IBucketService, BucketService>();
        }
    }
}