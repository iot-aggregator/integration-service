﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IntegrationService.Application.Repositories;
using IntegrationService.Application.Services;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Events;
using MassTransit;
using Microsoft.Extensions.Caching.Memory;

namespace IntegrationService.Infrastructure.Consumers
{
    public class PinMeasuredConsumer : IConsumer<IPinMeasured>
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IMeasurementPublisher _measurementPublisher;
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly int _absoluteCacheExpirationInSeconds;

        public PinMeasuredConsumer(IMemoryCache memoryCache, IMeasurementPublisher measurementPublisher, IRaspberryRepository raspberryRepository)
        {
            _memoryCache = memoryCache;
            _measurementPublisher = measurementPublisher;
            _raspberryRepository = raspberryRepository;

            var absoluteCacheExpirationInSeconds = Environment.GetEnvironmentVariable("ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS") ??
                                                   throw new ArgumentNullException(nameof(_absoluteCacheExpirationInSeconds),
                                                       "ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS environment variable must be set.");
            if (!int.TryParse(absoluteCacheExpirationInSeconds, out var parsedAbsoluteCacheExpirationInSeconds))
            {
                throw new ArgumentException("ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS environment variable must be an integer.");
            }

            _absoluteCacheExpirationInSeconds = parsedAbsoluteCacheExpirationInSeconds;
        }

        public async Task Consume(ConsumeContext<IPinMeasured> context)
        {
            var message = context.Message;

            if (!_memoryCache.TryGetValue(message.InstanceId, out Raspberry raspberry))
            {
                raspberry = await _raspberryRepository.GetRaspberryWithDevicesAsync(message.InstanceId);
                _memoryCache.Set(message.InstanceId, raspberry, new MemoryCacheEntryOptions
                {
                    AbsoluteExpiration = DateTimeOffset.UtcNow.AddSeconds(_absoluteCacheExpirationInSeconds)
                });
            }
            var sensor = raspberry.Sensors.First(x => message.PinName == x.PinName);

            var tags = new List<(string key, string value)>
            {
                (nameof(message.InstanceId), message.InstanceId.ToString()),
                (nameof(message.MeasuredAt), message.MeasuredAt.ToString()),
                (nameof(message.PinName), message.PinName),
                (nameof(sensor.Name), sensor.Name)
            };
            tags.AddRange(message.Tags.Select(x => (key: x.Key, value: x.Value)));

            await _measurementPublisher.PublishGaugeMeasurementAsync(message.InstanceId.ToString(),
                sensor.Name, sensor.MeasurementType,
                message.MeasuredValue, tags.ToArray());
        }
    }
}