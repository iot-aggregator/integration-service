﻿using System;
using IntegrationService.Domain.Common;

namespace IntegrationService.Domain.Entities
{
    public class Sensor : Device
    {
        public string MeasurementType { get; }
        public SensorType Type { get; }
        public double CoefficientA { get; }
        public double CoefficientB { get; }

        public Sensor(Guid id, Guid raspberryId, string pinName, string name, string description, PinStatus pinStatus,
            string measurementType, SensorType type, double coefficientA, double coefficientB) : base(id, raspberryId,
            pinName, name, description, pinStatus)
        {
            MeasurementType = measurementType;
            Type = type;
            CoefficientA = coefficientA;
            CoefficientB = coefficientB;
        }

        public static string DeviceType => DeviceTypes.Sensor;
    }
}