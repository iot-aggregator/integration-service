﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace IntegrationService.Domain.Entities
{
    public class Raspberry
    {
        public Guid Id { get; }
        public string Name { get; }
        public int Tube { get; }
        
        public ICollection<DeviceController> Controllers { get; }
        public ICollection<Sensor> Sensors { get; }

        public Raspberry(Guid id, string name, int tube)
        {
            Id = id;
            Name = name;
            Tube = tube;

            Controllers = new List<DeviceController>();
            Sensors = new List<Sensor>();
        }

        public Raspberry(Guid id, string name, int tube, ICollection<DeviceController> controllers, ICollection<Sensor> sensors)
        {
            Id = id;
            Name = name;
            Tube = tube;

            Controllers = controllers ?? new List<DeviceController>();
            Sensors = sensors ?? new List<Sensor>();
        }

        public void AddController(DeviceController deviceController)
        {
            Controllers.Add(deviceController);
        }

        public void AddSensor(Sensor sensor)
        {
            Sensors.Add(sensor);
        }

    }
}