﻿using System;
using IntegrationService.Domain.Common;

namespace IntegrationService.Domain.Entities
{
    public class DeviceController : Device
    {
        public TimeSpan WaitingTime { get; }
        public TimeSpan ActionTime { get; }

        public DeviceController(Guid id, Guid raspberryId, string pinName, string name, string description,
            PinStatus pinStatus, TimeSpan waitingTime, TimeSpan actionTime) : base(id, raspberryId, pinName, name,
            description, pinStatus)
        {
            WaitingTime = waitingTime;
            ActionTime = actionTime;
        }

        public static string DeviceType  => DeviceTypes.Controller;
    }
}