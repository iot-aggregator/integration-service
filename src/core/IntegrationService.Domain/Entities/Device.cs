﻿using System;
using IntegrationService.Domain.Common;

namespace IntegrationService.Domain.Entities
{
    public abstract class Device
    {
        public Guid Id { get; }
        public Guid RaspberryId { get; }
        public string PinName { get; }
        public string Name { get; }
        public string Description { get; }
        public PinStatus PinStatus { get; }

        protected Device(Guid id, Guid raspberryId, string pinName, string name, string description, PinStatus pinStatus)
        {
            Id = id;
            RaspberryId = raspberryId;
            PinName = pinName;
            Name = name;
            Description = description;
            PinStatus = pinStatus;
        }
    }
}