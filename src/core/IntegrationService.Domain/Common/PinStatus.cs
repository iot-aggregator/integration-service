﻿namespace IntegrationService.Domain.Common
{
    public enum PinStatus
    {
        Inactive,
        Active
    }
}