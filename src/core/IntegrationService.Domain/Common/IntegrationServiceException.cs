﻿using System;

namespace IntegrationService.Domain.Common
{
    public abstract class IntegrationServiceException : Exception
    {
        protected IntegrationServiceException(string message) : base(message)
        {
            
        }
    }
}