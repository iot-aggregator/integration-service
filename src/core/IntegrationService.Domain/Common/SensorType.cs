﻿namespace IntegrationService.Domain.Common
{
    public enum SensorType
    {
        Digital,
        Analog
    }
}