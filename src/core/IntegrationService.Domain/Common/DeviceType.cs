﻿namespace IntegrationService.Domain.Common
{
    public static class DeviceTypes
    {
        public static string Controller => "controller";
        public static string Sensor => "sensor";
    }
}