﻿using IntegrationService.Domain.Common;

namespace IntegrationService.Domain.Exceptions
{
    public class NotFoundException : IntegrationServiceException
    {
        public NotFoundException(string message) : base(message)
        {
        }
    }
}