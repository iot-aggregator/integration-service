﻿using System;
using IntegrationService.Application.Behaviors;
using IntegrationService.Application.Common;
using IntegrationService.Application.Raspberries.Commands.CreateRaspberry;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.DependencyInjection;

namespace IntegrationService.Application.Installers
{
    public static class MediatRInstaller
    {
        /// <summary>
        ///   Installs MediatR with Commmands, Handlers and PipelineBehaviors
        /// </summary>
        /// <param name="services">IServiceCollection selected for MediatR configuration</param>
        /// <returns>The same IServiceCollection with installed MediatR</returns>
        public static IServiceCollection InstallMediatR(this IServiceCollection services, RaspberryUpdaterMode raspberryUpdaterMode = RaspberryUpdaterMode.None)
        {
            services.AddMediatR(typeof(CreateRaspberryCommand));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(BasicExceptionHandler<,>));

            if (raspberryUpdaterMode == RaspberryUpdaterMode.Consul)
            {
                throw new NotImplementedException();
            }
            else if (raspberryUpdaterMode == RaspberryUpdaterMode.UrlTemplate)
            {
                services.CheckDefaultUrlTemplateVariables();
                services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UrlTemplateRaspberryUpdateBehavior<,>));
            }

            return services;
        }

        public static IServiceCollection CheckDefaultUrlTemplateVariables(this IServiceCollection services)
        {
            var templatePlaceholder = Environment.GetEnvironmentVariable("PLACEHOLDER_URL_TEMPLATE");
            if (templatePlaceholder is null)
            {
                Environment.SetEnvironmentVariable("PLACEHOLDER_URL_TEMPLATE", "stationId");
            }

            var urlTemplate = Environment.GetEnvironmentVariable("RASPBERRY_URL_TEMPLATE");
            if (urlTemplate is null)
            {
                throw new ArgumentNullException("RASPBERRY_URL_TEMPLATE", "RASPBERRY_URL_TEMPLATE environment variable must be set in RaspberryUpdaterMode.UrlTemplate mode.");
            }

            return services;
        }
    }
}