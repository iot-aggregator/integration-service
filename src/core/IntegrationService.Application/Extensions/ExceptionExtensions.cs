﻿using System;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Exceptions;

namespace IntegrationService.Application.Extensions
{
    public static class ExceptionExtensions
    {
        public static bool IsDomainException(this Exception exception) => exception is IntegrationServiceException;

        public static bool IsNotFoundDomainException(this Exception exception) => exception is NotFoundException;
    }
}