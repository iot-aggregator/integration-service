﻿using System;
using System.Threading.Tasks;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Exceptions;

namespace IntegrationService.Application.Extensions
{
    public static class RaspberryRepositoryExtensions
    {
        public static async Task ThrowNotFoundIfRaspberryDoesNotExist(this IRaspberryRepository raspberryRepository, Guid id)
        {
            var raspberry = await raspberryRepository.GetRaspberryAsync(id);

            if (raspberry == null)
            {
                throw new NotFoundException("Raspberry Pi not found");
            }
        }

        public static async Task ThrowNotFoundIfSensorDoesNotExist(this IRaspberryRepository raspberryRepository,
            Guid id)
        {
            var sensor = await raspberryRepository.GetSensorAsync(id);

            if (sensor == null)
            {
                throw new NotFoundException("Sensor not found");
            }
        }

        public static async Task ThrowNotFoundIfDeviceControllerDoesNotExist(this IRaspberryRepository raspberryRepository,
            Guid id)
        {
            var deviceController = await raspberryRepository.GetDeviceControllerAsync(id);

            if (deviceController == null)
            {
                throw new NotFoundException("Controller not found");
            }
        }
    }
}