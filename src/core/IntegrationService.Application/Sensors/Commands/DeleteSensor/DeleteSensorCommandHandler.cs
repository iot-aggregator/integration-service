﻿using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using MediatR;

namespace IntegrationService.Application.Sensors.Commands.DeleteSensor
{
    public class DeleteSensorCommandHandler : IRequestHandler<DeleteSensorCommand, Unit>
    {
        private readonly IRaspberryRepository _raspberryRepository;

        public DeleteSensorCommandHandler(IRaspberryRepository raspberryRepository)
        {
            _raspberryRepository = raspberryRepository;
        }
        public async Task<Unit> Handle(DeleteSensorCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);
            await _raspberryRepository.ThrowNotFoundIfSensorDoesNotExist(request.Id);
            
            await _raspberryRepository.DeleteDeviceAsync(request.Id);

            return Unit.Value;;
        }
    }
}