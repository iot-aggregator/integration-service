﻿using System;
using IntegrationService.Application.Common;
using MediatR;

namespace IntegrationService.Application.Sensors.Commands.DeleteSensor
{
    public class DeleteSensorCommand : IRequest<Unit>, IRaspberryUpdater
    {
        public Guid Id { get; set; }
        public Guid RpiId { get; set; }
    }
}