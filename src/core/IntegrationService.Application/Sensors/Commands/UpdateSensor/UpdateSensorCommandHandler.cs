﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Entities;
using MediatR;

namespace IntegrationService.Application.Sensors.Commands.UpdateSensor
{
    public class UpdateSensorCommandHandler : IRequestHandler<UpdateSensorCommand, Unit>
    {
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly IMapper _mapper;

        public UpdateSensorCommandHandler(IRaspberryRepository raspberryRepository, IMapper mapper)
        {
            _raspberryRepository = raspberryRepository;
            _mapper = mapper;
        }
        public async Task<Unit> Handle(UpdateSensorCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);
            await _raspberryRepository.ThrowNotFoundIfSensorDoesNotExist(request.Id);

            var sensor = _mapper.Map<Sensor>(request);

            await _raspberryRepository.UpdateSensorAsync(sensor);

            return Unit.Value;
        }
    }
}