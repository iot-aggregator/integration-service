﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Entities;
using IntegrationService.Domain.Exceptions;
using MediatR;

namespace IntegrationService.Application.Sensors.Commands.CreateSensor
{
    public class CreateSensorCommandHandler : IRequestHandler<CreateSensorCommand, Guid>
    {
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly IMapper _mapper;

        public CreateSensorCommandHandler(IRaspberryRepository raspberryRepository, IMapper mapper)
        {
            _raspberryRepository = raspberryRepository;
            _mapper = mapper;
        }
        public async Task<Guid> Handle(CreateSensorCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);

            var sensor = _mapper.Map<Sensor>(request);

            await _raspberryRepository.AddSensorAsync(sensor);

            return sensor.Id;
        }
    }
}