﻿using System;
using IntegrationService.Application.Common;
using MediatR;

namespace IntegrationService.Application.Sensors.Commands.CreateSensor
{
    public class CreateSensorCommand : IRequest<Guid>, IRaspberryUpdater
    {
        public Guid RpiId { get; set; }
        public string PinName { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MeasurementType { get; set; }
        public string Type { get; set; }
        public double CoefficientA { get; set; }
        public double CoefficientB { get; set; }
    }
}