﻿using System.Collections.Generic;

namespace IntegrationService.Application.Dtos
{
    public class RaspberryConfigurationDto
    {
        public List<SensorDto> Sensors { get; set; }
        public List<DeviceControllerDto> Controllers { get; set; }
    }
}