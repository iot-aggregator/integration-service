﻿using System;

namespace IntegrationService.Application.Dtos
{
    public class DeviceControllerDto
    {
        public Guid Id { get; set; }
        public string PinName { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int WaitingTimeInSeconds { get; set; }
        public int ActionTimeInSeconds { get; set; }
    }
}