﻿using System;

namespace IntegrationService.Application.Dtos
{
    public class RaspberryBlockDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Tube { get; set; }
    }
}