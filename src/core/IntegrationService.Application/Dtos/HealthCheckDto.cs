﻿using System;

namespace IntegrationService.Application.Dtos
{
    public class HealthCheckDto
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Method { get; set; }
        public TimeSpan Interval { get; set; }
        public TimeSpan Timeout { get; set; }
    }
}