﻿using System;
using System.Collections;

namespace IntegrationService.Application.Dtos
{
    public class RaspberryDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Tube { get; set; }
        public RaspberryConfigurationDto Config { get; set; }
    }
}