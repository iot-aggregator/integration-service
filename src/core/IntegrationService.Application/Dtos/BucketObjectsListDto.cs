﻿using System.Collections.Generic;

namespace IntegrationService.Application.Dtos
{
    public class BucketObjectsListDto
    {
        public IEnumerable<string> Links { get; set; }
        public string ContinuationToken { get; set; }
    }
}