﻿using Microsoft.AspNetCore.Http;

namespace IntegrationService.Application.Dtos
{
    public class ImageUploadDto
    {
        public IFormFile Image { get; set; }
        public string Key { get; set; }
    }
}