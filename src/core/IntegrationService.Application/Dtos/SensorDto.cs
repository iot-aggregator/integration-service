﻿using System;

namespace IntegrationService.Application.Dtos
{
    public class SensorDto
    {
        public Guid Id { get; set; }
        public string PinName { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MeasurementType { get; set; }
        public string SensorType { get; set; }
        public double CoefficientA { get; set; }
        public double CoefficientB { get; set; }
    }
}