﻿namespace IntegrationService.Application.Common
{
    public enum RaspberryUpdaterMode
    {
        None = 0,
        Consul,
        UrlTemplate
    }
}