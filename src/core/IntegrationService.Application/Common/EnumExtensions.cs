﻿using System;

namespace IntegrationService.Application.Common
{
    public static class EnumExtensions
    {
        /// <summary>
        ///   Extension method that tries to parse string to Enum of <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T">Type of Enum to parse <paramref name="source"/> to</typeparam>
        /// <param name="source">Source string to be parsed</param>
        /// <param name="ignoreCase">Flag that specifies if case of <paramref name="source"/> is ignored</param>
        /// <returns>Parsed Enum value</returns>
        /// <exception cref="ArgumentException">
        ///   When <paramref name="source"/> can not be parsed to <typeparamref name="T"/> Enum.
        /// </exception>
        public static T ParseOrFail<T>(this string source, bool ignoreCase = true) where T : struct, Enum
        {
            if (!Enum.TryParse(source, ignoreCase, out T parsed))
            {
                throw new ArgumentException($"Can not parse {source} to Enum of type {nameof(T)}");
            }

            return parsed;
        }

        /// <summary>
        ///   Extension method that parses Enum value to lower invariant string
        /// </summary>
        /// <typeparam name="T">Type of Enum which <paramref name="value"/> is cast to string</typeparam>
        /// <param name="value">Value of Enum of type <typeparamref name="T"/></param>
        /// <returns><paramref name="value"/> in lower invariant string</returns>
        public static string ToLowerInvariantString<T>(this T value) where T : Enum
        {
            return value.ToString().ToLowerInvariant();
        }

        public static RaspberryUpdaterMode GetUpdaterModeByString(this string value)
        {
            return value?.ToLowerInvariant() switch
            {
                "consul" => RaspberryUpdaterMode.Consul,
                "url_template" => RaspberryUpdaterMode.UrlTemplate,
                null => RaspberryUpdaterMode.None,
                _ => throw new ArgumentOutOfRangeException(nameof(value), "Value of RASPBERRY_UPDATER_MODE environemnt variable is not handled.")
            };
        }
    }
}