﻿using System;

namespace IntegrationService.Application.Common
{
    /// <summary>
    ///   Marker interface for RaspberryUpdaterBehaviors
    /// </summary>
    public interface IRaspberryUpdater
    {
        public Guid RpiId { get; }
    }
}