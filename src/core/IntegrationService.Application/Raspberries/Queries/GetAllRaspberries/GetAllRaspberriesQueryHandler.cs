﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Repositories;
using MediatR;

namespace IntegrationService.Application.Raspberries.Queries.GetAllRaspberries
{
    public class GetAllRaspberriesQueryHandler : IRequestHandler<GetAllRaspberriesQuery, IEnumerable<RaspberryBlockDto>>
    {
        private readonly IMapper _mapper;
        private readonly IRaspberryRepository _raspberryRepository;

        public GetAllRaspberriesQueryHandler(IMapper mapper, IRaspberryRepository raspberryRepository)
        {
            _mapper = mapper;
            _raspberryRepository = raspberryRepository;
        }
        public async Task<IEnumerable<RaspberryBlockDto>> Handle(GetAllRaspberriesQuery request, CancellationToken cancellationToken)
        {
            var raspberries = await _raspberryRepository.GetRaspberriesAsync();

            var dtos = _mapper.Map<IEnumerable<RaspberryBlockDto>>(raspberries);

            return dtos;
        }
    }
}