﻿using System.Collections.Generic;
using IntegrationService.Application.Dtos;
using MediatR;

namespace IntegrationService.Application.Raspberries.Queries.GetAllRaspberries
{
    public class GetAllRaspberriesQuery : IRequest<IEnumerable<RaspberryBlockDto>>
    {
        
    }
}