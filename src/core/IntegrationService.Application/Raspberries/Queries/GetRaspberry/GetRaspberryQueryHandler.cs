﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using MediatR;

namespace IntegrationService.Application.Raspberries.Queries.GetRaspberry
{
    public class GetRaspberryQueryHandler : IRequestHandler<GetRaspberryQuery, RaspberryDto>
    {
        private readonly IMapper _mapper;
        private readonly IRaspberryRepository _raspberryRepository;

        public GetRaspberryQueryHandler(IMapper mapper, IRaspberryRepository raspberryRepository)
        {
            _mapper = mapper;
            _raspberryRepository = raspberryRepository;
        }
        public async Task<RaspberryDto> Handle(GetRaspberryQuery request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.Id);
            var raspberry = await _raspberryRepository.GetRaspberryWithDevicesAsync(request.Id);

            var dto = _mapper.Map<RaspberryDto>(raspberry);

            return dto;
        }
    }
}