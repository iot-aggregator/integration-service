﻿using System;
using IntegrationService.Application.Dtos;
using MediatR;

namespace IntegrationService.Application.Raspberries.Queries.GetRaspberry
{
    public class GetRaspberryQuery : IRequest<RaspberryDto>
    {
        public Guid Id { get; set; }
    }
}