﻿using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using MediatR;

namespace IntegrationService.Application.Raspberries.Commands.DeleteRaspberry
{
    public class DeleteRaspberryCommandHandler : IRequestHandler<DeleteRaspberryCommand, Unit>
    {
        private readonly IRaspberryRepository _raspberryRepository;

        public DeleteRaspberryCommandHandler(IRaspberryRepository raspberryRepository)
        {
            _raspberryRepository = raspberryRepository;
        }
        public async Task<Unit> Handle(DeleteRaspberryCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);

            await _raspberryRepository.DeleteRaspberryAsync(request.RpiId);

            return Unit.Value;
        }
    }
}