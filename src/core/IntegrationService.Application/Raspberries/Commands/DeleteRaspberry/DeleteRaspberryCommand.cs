﻿using System;
using IntegrationService.Application.Common;
using MediatR;

namespace IntegrationService.Application.Raspberries.Commands.DeleteRaspberry
{
    public class DeleteRaspberryCommand : IRequest<Unit>, IRaspberryUpdater
    {
        public Guid RpiId { get; set; }
    }
}