﻿using System;
using MediatR;

namespace IntegrationService.Application.Raspberries.Commands.CreateRaspberry
{
    public class CreateRaspberryCommand : IRequest<Guid>
    {
        public string Name { get; set; }
        public int Tube { get; set; }
    }
}