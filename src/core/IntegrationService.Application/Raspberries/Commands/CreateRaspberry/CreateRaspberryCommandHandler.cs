﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Entities;
using MediatR;

namespace IntegrationService.Application.Raspberries.Commands.CreateRaspberry
{
    public class CreateRaspberryCommandHandler : IRequestHandler<CreateRaspberryCommand, Guid>
    {
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly IMapper _mapper;

        public CreateRaspberryCommandHandler(IRaspberryRepository raspberryRepository, IMapper mapper)
        {
            _raspberryRepository = raspberryRepository;
            _mapper = mapper;
        }
        public async Task<Guid> Handle(CreateRaspberryCommand request, CancellationToken cancellationToken)
        {
            var raspberry = _mapper.Map<Raspberry>(request);

            await _raspberryRepository.AddRaspberryAsync(raspberry);

            return raspberry.Id;
        }
    }
}