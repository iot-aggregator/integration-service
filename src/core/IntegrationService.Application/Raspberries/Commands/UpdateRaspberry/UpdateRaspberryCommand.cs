﻿using System;
using IntegrationService.Application.Common;
using MediatR;

namespace IntegrationService.Application.Raspberries.Commands.UpdateRaspberry
{
    public class UpdateRaspberryCommand : IRequest<Unit>, IRaspberryUpdater
    {
        public Guid RpiId { get; set; }
        public string Name { get; set; }
        public int Tube { get; set; }
    }
}