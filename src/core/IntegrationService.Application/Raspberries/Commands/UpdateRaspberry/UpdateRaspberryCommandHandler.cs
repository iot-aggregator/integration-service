﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Entities;
using MediatR;

namespace IntegrationService.Application.Raspberries.Commands.UpdateRaspberry
{
    public class UpdateRaspberryCommandHandler : IRequestHandler<UpdateRaspberryCommand, Unit>
    {
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly IMapper _mapper;

        public UpdateRaspberryCommandHandler(IRaspberryRepository raspberryRepository, IMapper mapper)
        {
            _raspberryRepository = raspberryRepository;
            _mapper = mapper;
        }
        public async Task<Unit> Handle(UpdateRaspberryCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);

            var raspberry = _mapper.Map<Raspberry>(request);

            await _raspberryRepository.UpdateRaspberryAsync(raspberry);

            return Unit.Value;
        }
    }
}