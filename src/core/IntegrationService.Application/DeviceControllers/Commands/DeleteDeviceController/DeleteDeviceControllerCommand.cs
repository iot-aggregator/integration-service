﻿using System;
using IntegrationService.Application.Common;
using MediatR;

namespace IntegrationService.Application.DeviceControllers.Commands.DeleteDeviceController
{
    public class DeleteDeviceControllerCommand : IRequest<Unit>, IRaspberryUpdater
    {
        public Guid Id { get; set; }
        public Guid RpiId { get; set; }
    }
}