﻿using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using MediatR;

namespace IntegrationService.Application.DeviceControllers.Commands.DeleteDeviceController
{
    public class DeleteDeviceControllerCommandHandler : IRequestHandler<DeleteDeviceControllerCommand, Unit>
    {
        private readonly IRaspberryRepository _raspberryRepository;

        public DeleteDeviceControllerCommandHandler(IRaspberryRepository raspberryRepository)
        {
            _raspberryRepository = raspberryRepository;
        }

        public async Task<Unit> Handle(DeleteDeviceControllerCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);
            await _raspberryRepository.ThrowNotFoundIfDeviceControllerDoesNotExist(request.Id);

            await _raspberryRepository.DeleteDeviceAsync(request.Id);

            return Unit.Value;
        }
    }
}