﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Entities;
using MediatR;

namespace IntegrationService.Application.DeviceControllers.Commands.CreateDeviceController
{
    public class CreateDeviceControllerCommandHandler : IRequestHandler<CreateDeviceControllerCommand, Guid>
    {
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly IMapper _mapper;

        public CreateDeviceControllerCommandHandler(IRaspberryRepository raspberryRepository, IMapper mapper)
        {
            _raspberryRepository = raspberryRepository;
            _mapper = mapper;
        }
        public async Task<Guid> Handle(CreateDeviceControllerCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);

            var deviceController = _mapper.Map<DeviceController>(request);

            await _raspberryRepository.AddDeviceControllerAsync(deviceController);

            return deviceController.Id;
        }
    }
}