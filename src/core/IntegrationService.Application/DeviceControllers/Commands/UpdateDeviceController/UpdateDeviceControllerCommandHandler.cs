﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Extensions;
using IntegrationService.Application.Repositories;
using IntegrationService.Domain.Entities;
using MediatR;

namespace IntegrationService.Application.DeviceControllers.Commands.UpdateDeviceController
{
    public class UpdateDeviceControllerCommandHandler : IRequestHandler<UpdateDeviceControllerCommand, Unit>
    {
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly IMapper _mapper;

        public UpdateDeviceControllerCommandHandler(IRaspberryRepository raspberryRepository, IMapper mapper)
        {
            _raspberryRepository = raspberryRepository;
            _mapper = mapper;
        }
        public async Task<Unit> Handle(UpdateDeviceControllerCommand request, CancellationToken cancellationToken)
        {
            await _raspberryRepository.ThrowNotFoundIfRaspberryDoesNotExist(request.RpiId);
            await _raspberryRepository.ThrowNotFoundIfDeviceControllerDoesNotExist(request.Id);

            var deviceController = _mapper.Map<DeviceController>(request);

            await _raspberryRepository.UpdateDeviceControllerAsync(deviceController);

            return Unit.Value;
        }
    }
}