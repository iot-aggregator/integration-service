﻿using System;
using IntegrationService.Application.Common;
using MediatR;

namespace IntegrationService.Application.DeviceControllers.Commands.UpdateDeviceController
{
    public class UpdateDeviceControllerCommand : IRequest<Unit>, IRaspberryUpdater
    {
        public Guid Id { get; set; }
        public Guid RpiId { get; set; }
        public string PinName { get; set; }
        public string Status { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int WaitingTimeInSeconds { get; set; }
        public int ActionTimeInSeconds { get; set; }
    }
}