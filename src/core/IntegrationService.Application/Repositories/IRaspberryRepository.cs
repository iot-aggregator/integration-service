﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IntegrationService.Domain.Entities;

namespace IntegrationService.Application.Repositories
{
    public interface IRaspberryRepository
    {
        /// <summary>
        ///   Adds Raspberry without its devices
        /// </summary>
        /// <param name="raspberry">Raspberry entity to be added</param>
        /// <returns>Task</returns>
        Task AddRaspberryAsync(Raspberry raspberry);

        /// <summary>
        ///   Retrieves all Raspberries
        /// </summary>
        /// <returns>Task with IEnumerable of Raspberries</returns>
        Task<IEnumerable<Raspberry>> GetRaspberriesAsync();

        /// <summary>
        ///   Retrieves Raspberry without its devices by Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with Raspberry without its devices</returns>
        Task<Raspberry> GetRaspberryAsync(Guid id);

        /// <summary>
        ///   Retrieves Raspberry with its devices by Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with Raspberry and its devices</returns>
        Task<Raspberry> GetRaspberryWithDevicesAsync(Guid id);

        /// <summary>
        ///   Updates Raspberry
        /// </summary>
        /// <param name="raspberry">Raspberry to be updated</param>
        /// <returns>Task</returns>
        Task UpdateRaspberryAsync(Raspberry raspberry);

        /// <summary>
        ///   Deletes Raspberry
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task</returns>
        Task DeleteRaspberryAsync(Guid id);

        /// <summary>
        ///   Adds DeviceController
        /// </summary>
        /// <param name="deviceController">DeviceController to be added</param>
        /// <returns>Task</returns>
        Task AddDeviceControllerAsync(DeviceController deviceController);

        /// <summary>
        ///   Retrieves DeviceControllers of Raspberry with specified Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with IEnumerable of Raspberry's DeviceControllers</returns>
        Task<IEnumerable<DeviceController>> GetRaspberryDeviceControllersAsync(Guid id);

        /// <summary>
        ///   Retrieves DeviceController by its Id
        /// </summary>
        /// <param name="id">DeviceController Id</param>
        /// <returns>Task with DeviceController</returns>
        Task<DeviceController> GetDeviceControllerAsync(Guid id);

        /// <summary>
        ///   Updates DeviceController
        /// </summary>
        /// <param name="deviceController">DeviceController to be updated</param>
        /// <returns>Task</returns>
        Task UpdateDeviceControllerAsync(DeviceController deviceController);

        /// <summary>
        ///   Adds Sensor
        /// </summary>
        /// <param name="sensor">Sensor to be added</param>
        /// <returns>Task</returns>
        Task AddSensorAsync(Sensor sensor);

        /// <summary>
        ///   Retrieves Sensors of Raspberry with specified Id
        /// </summary>
        /// <param name="id">Raspberry Id</param>
        /// <returns>Task with IEnumerable of Sensors</returns>
        Task<IEnumerable<Sensor>> GetRaspberrySensorsAsync(Guid id);

        /// <summary>
        ///   Retrieves Sensor by its Id
        /// </summary>
        /// <param name="id">Sensor id</param>
        /// <returns>Task with Sensor</returns>
        Task<Sensor> GetSensorAsync(Guid id);

        /// <summary>
        ///   Updates Sensor
        /// </summary>
        /// <param name="sensor">Sensor to be updated</param>
        /// <returns>Task</returns>
        Task UpdateSensorAsync(Sensor sensor);

        /// <summary>
        ///   Deletes Device (DeviceController or Sensor)
        /// </summary>
        /// <param name="id">Device Id</param>
        /// <returns>Task</returns>
        Task DeleteDeviceAsync(Guid id);

        /// <summary>
        ///   Clears both tables
        /// </summary>
        /// <returns>Task</returns>
        Task ClearDatabase();
    }
}