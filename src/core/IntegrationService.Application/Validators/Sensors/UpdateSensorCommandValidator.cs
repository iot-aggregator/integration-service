﻿using System;
using System.Linq;
using FluentValidation;
using IntegrationService.Application.Common;
using IntegrationService.Application.Repositories;
using IntegrationService.Application.Sensors.Commands.UpdateSensor;
using IntegrationService.Application.Validators.Common;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using Microsoft.Extensions.Localization;

namespace IntegrationService.Application.Validators.Sensors
{
    public class UpdateSensorCommandValidator : AbstractValidator<UpdateSensorCommand>
    {
        public UpdateSensorCommandValidator(IRaspberryRepository raspberryRepository, IStringLocalizer<ApplicationLanguage> localizer)
        {
            RuleFor(x => x)
                .MustAsync(async (command, token) =>
                {
                    if (command.RpiId == default)
                    {
                        return true;
                    }

                    return await CommonValidatorResources.CheckNameAndPinNameUniquenessForExistingDevice((raspberryRepository, command.RpiId,
                        command.Id, command.Name, command.PinName, token));
                })
                .WithMessage(command => localizer["\"{0}\" and \"{1}\" must be unique values for Raspberry Pi", nameof(command.PinName), nameof(command.Name)]);

            RuleFor(x => x.PinName)
                .Matches(CommonValidatorResources.Pattern)
                .WithMessage(command => localizer["\"{0}\" must match template \"GPIO<number>\" or \"ANALOG<number>\"", nameof(command.PinName)]);

            RuleFor(x => x.Status)
                .Must(s => Enum.TryParse(s, true, out PinStatus _))
                .WithMessage(command => localizer["Invalid \"{0}\" value", nameof(command.Status)]);

            RuleFor(x => x.Type)
                .Must(s => Enum.TryParse(s, true, out SensorType _))
                .WithMessage(command => localizer["Invalid \"{0}\" value", nameof(command.Type)]);

            RuleFor(x => x.Type)
                .Must(s => Enum.TryParse(s, true, out SensorType _))
                .WithMessage(command => localizer["Invalid \"{0}\" value", nameof(command.Type)])
                .DependentRules(() =>
                {
                    RuleFor(x => x)
                        .Must(command =>
                        {
                            return CommonValidatorResources.CheckPinNameToSensorTypeMatching((command.PinName, command.Type,
                                new[]
                                {
                                    (CommonValidatorResources.GpioPattern, SensorType.Digital),
                                    (CommonValidatorResources.AnalogPattern, SensorType.Analog)
                                }));
                        })
                        .WithMessage(command => localizer["\"{0}\" must match provided \"{1}\"", nameof(command.PinName), nameof(command.Type)]);
                });
        }
    }
}