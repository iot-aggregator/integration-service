﻿using FluentValidation;
using IntegrationService.Application.Raspberries.Commands.CreateRaspberry;

namespace IntegrationService.Application.Validators.Raspberries
{
    public class CreateRaspberryCommandValidator : AbstractValidator<CreateRaspberryCommand>
    {
        public CreateRaspberryCommandValidator()
        {
            RuleFor(x => x.Tube)
                .GreaterThanOrEqualTo(0);
        }
    }
}