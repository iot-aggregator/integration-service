﻿using FluentValidation;
using IntegrationService.Application.Common;
using IntegrationService.Application.Raspberries.Commands.UpdateRaspberry;
using IntegrationService.Application.Repositories;
using Microsoft.Extensions.Localization;

namespace IntegrationService.Application.Validators.Raspberries
{
    public class UpdateRaspberryCommandValidator : AbstractValidator<UpdateRaspberryCommand>
    {
        public UpdateRaspberryCommandValidator(IRaspberryRepository raspberryRepository, IStringLocalizer<ApplicationLanguage> localizer)
        {
            RuleFor(x => x.Tube)
                .GreaterThanOrEqualTo(0);
        }
    }
}