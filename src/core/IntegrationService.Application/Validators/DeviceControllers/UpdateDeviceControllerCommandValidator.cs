﻿using System;
using System.Linq;
using FluentValidation;
using IntegrationService.Application.Common;
using IntegrationService.Application.DeviceControllers.Commands.UpdateDeviceController;
using IntegrationService.Application.Repositories;
using IntegrationService.Application.Validators.Common;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using Microsoft.Extensions.Localization;

namespace IntegrationService.Application.Validators.DeviceControllers
{
    public class UpdateDeviceControllerCommandValidator : AbstractValidator<UpdateDeviceControllerCommand>
    {
        public UpdateDeviceControllerCommandValidator(IRaspberryRepository raspberryRepository, IStringLocalizer<ApplicationLanguage> localizer)
        {
            RuleFor(x => x)
                .MustAsync(async (command, token) =>
                {
                    if (command.RpiId == default)
                    {
                        return true;
                    }

                    return await CommonValidatorResources.CheckNameAndPinNameUniquenessForExistingDevice((raspberryRepository, command.RpiId,
                        command.Id, command.Name, command.PinName, token));
                })
                .WithMessage(command => localizer["\"{0}\" and \"{1}\" must be unique values for Raspberry Pi", nameof(command.PinName), nameof(command.Name)]);

            RuleFor(x => x.PinName)
                .Matches(CommonValidatorResources.GpioPattern)
                .WithMessage(command => localizer["\"{0}\" must match template \"GPIO<number>\"", nameof(command.PinName)]);

            RuleFor(x => x.Status)
                .Must(s => Enum.TryParse(s, true, out PinStatus _))
                .WithMessage(command => localizer["Invalid \"{0}\" value", nameof(command.Status)]);

            RuleFor(x => x.WaitingTimeInSeconds)
                .GreaterThan(0);

            RuleFor(x => x.ActionTimeInSeconds)
                .GreaterThan(0);
        }
    }
}