﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Common;
using IntegrationService.Application.Repositories;
using IntegrationService.Application.Sensors.Commands.CreateSensor;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;

namespace IntegrationService.Application.Validators.Common
{
    public static class CommonValidatorResources
    {
        public const string Pattern = @"(ANALOG|GPIO)\d+";
        public const string GpioPattern = @"GPIO\d+";
        public const string AnalogPattern = @"ANALOG\d+";

        private static Func<(IEnumerable<Device> devices, string name, string pinName), bool>
            TryFindNameOrPinNameInDevices =>
            tuple =>
            {
                var (devices, name, pinName) = tuple;
                return !devices.Any(x =>
                    x.PinName.Equals(pinName, StringComparison.InvariantCultureIgnoreCase)
                    || x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
            };

        public static Func<(IRaspberryRepository repository, Guid rpiId, Guid deviceId, string name, string pinName, CancellationToken token), Task<bool>>
            CheckNameAndPinNameUniquenessForExistingDevice =>
            async tuple =>
            {
                var (repository, rpiId, deviceId, name, pinName, token) = tuple;
                var raspberry = await repository.GetRaspberryWithDevicesAsync(rpiId);

                var devices = raspberry.Sensors.Select(x => (Device)x)
                    .Concat(raspberry.Controllers.Select(x => (Device)x))
                    .Where(x => x.Id != deviceId)
                    .ToList();

                return TryFindNameOrPinNameInDevices((devices, name, pinName));
            };

        public static Func<(IRaspberryRepository repository, Guid rpiId, string name, string pinName, CancellationToken token), Task<bool>>
            CheckNameAndPinNameUniqueness =>
            async tuple =>
            {
                var (repository, rpiId, name, pinName, _) = tuple;
                var raspberry = await repository.GetRaspberryWithDevicesAsync(rpiId);

                var devices = raspberry.Sensors.Select(x => (Device)x)
                    .Concat(raspberry.Controllers.Select(x => (Device)x)).ToList();

                return TryFindNameOrPinNameInDevices((devices, name, pinName));
            };

        public static Func<(string pinName, string sensorType, IEnumerable<(string pattern, SensorType matchingType)> patternsToTypes), bool>
            CheckPinNameToSensorTypeMatching => 
            tuple =>
            {
                var (pinName, sensorType, patternsToTypes) = tuple;
                var parsedSensorType = sensorType.ParseOrFail<SensorType>();
                var regexToTypes = patternsToTypes.Select(x => (regex: new Regex(x.pattern), x.matchingType));

                return regexToTypes.Any(x => x.regex.IsMatch(pinName) && parsedSensorType == x.matchingType);
            };
    }
}