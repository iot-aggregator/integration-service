﻿using System;
using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Extensions;
using MediatR;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;

namespace IntegrationService.Application.Behaviors
{
    /// <summary>
    ///   Basic exception handler that logs exceptions
    /// </summary>
    /// <typeparam name="TRequest">Type that is passed to IMediator</typeparam>
    /// <typeparam name="TResponse">Type that is returned when calling IMediator with <typeparamref name="TRequest"/></typeparam>
    public class BasicExceptionHandler<TRequest, TResponse>: IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogger<BasicExceptionHandler<TRequest, TResponse>> _logger;

        public BasicExceptionHandler(ILogger<BasicExceptionHandler<TRequest, TResponse>> logger)
        {
            _logger = logger;
        }
        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            try
            {
                return await next();
            }
            catch (Exception e)
            {
                _logger.LogError("{exception} - {exceptionMessage}", e, e.Message);
                throw;
            }
        }
    }
}