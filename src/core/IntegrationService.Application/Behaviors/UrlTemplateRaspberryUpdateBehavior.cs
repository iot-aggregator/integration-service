﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using IntegrationService.Application.Common;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Repositories;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace IntegrationService.Application.Behaviors
{
    /// <summary>
    ///   Updates Raspberry with new configuration using url template
    /// </summary>
    /// <typeparam name="TRequest">Type that is passed to IMediator</typeparam>
    /// <typeparam name="TResponse">Type that is returned when calling IMediator with <typeparamref name="TRequest"/></typeparam>
    public class UrlTemplateRaspberryUpdateBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse> where TRequest : IRaspberryUpdater
    {
        private readonly ILogger<UrlTemplateRaspberryUpdateBehavior<TRequest, TResponse>> _logger;
        private readonly IRaspberryRepository _raspberryRepository;
        private readonly IMapper _mapper;
        private readonly IMemoryCache _memoryCache;
        private readonly string _urlTemplate;
        private readonly HttpClient _client;
        private readonly string _placeholderUrlTemplate;
        private readonly int _absoluteCacheExpirationInSeconds;

        public UrlTemplateRaspberryUpdateBehavior(
            ILogger<UrlTemplateRaspberryUpdateBehavior<TRequest, TResponse>> logger,
            IRaspberryRepository raspberryRepository, IHttpClientFactory httpClientFactory, IMapper mapper,
            IMemoryCache memoryCache)
        {
            _logger = logger;
            _raspberryRepository = raspberryRepository;
            _mapper = mapper;
            _memoryCache = memoryCache;
            _urlTemplate = Environment.GetEnvironmentVariable("RASPBERRY_URL_TEMPLATE") ??
                           throw new ArgumentNullException(nameof(_urlTemplate),
                               "RASPBERRY_URL_TEMPLATE environment variable must be set in RaspberryUpdaterMode.UrlTemplate mode.");
            var placeholderUrlTemplate = Environment.GetEnvironmentVariable("PLACEHOLDER_URL_TEMPLATE") ??
                                         throw new ArgumentNullException(nameof(_urlTemplate),
                                             "PLACEHOLDER_URL_TEMPLATE environment variable must be set in RaspberryUpdaterMode.UrlTemplate mode.");
            _placeholderUrlTemplate = $"{{{placeholderUrlTemplate}}}";

            var absoluteCacheExpirationInSeconds = Environment.GetEnvironmentVariable("ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS") ??
                                                   throw new ArgumentNullException(nameof(_absoluteCacheExpirationInSeconds),
                                                       "ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS environment variable must be set.");
            if (!int.TryParse(absoluteCacheExpirationInSeconds, out var parsedAbsoluteCacheExpirationInSeconds))
            {
                throw new ArgumentException("ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS environment variable must be an integer.");
            }

            _absoluteCacheExpirationInSeconds = parsedAbsoluteCacheExpirationInSeconds;

            _client = httpClientFactory.CreateClient();
        }

        private static Action<ILogger> GetLoggerActionBasedOnResponseMessage(Guid raspberryId, HttpResponseMessage responseMessage) =>
            responseMessage.IsSuccessStatusCode switch
            {
                true => logger =>
                    logger.LogInformation("Sending update to Raspberry Pi station with Id: {rpiId} was successful.",
                        raspberryId),
                false => logger =>
                    logger.LogError(
                        "Sending update to station with Id: {rpiId} was unsuccessful, status code: {statusCode}",
                        raspberryId, (int)responseMessage.StatusCode)
            };

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            var response = await next();

            _logger.LogInformation("Sending update to Raspberry Pi station with Id: {rpiId}", request.RpiId);

            var raspberry = await _raspberryRepository.GetRaspberryWithDevicesAsync(request.RpiId);
            _memoryCache.Set(raspberry.Id, raspberry, new MemoryCacheEntryOptions
            {
                AbsoluteExpiration = DateTimeOffset.UtcNow.AddSeconds(_absoluteCacheExpirationInSeconds)
            });

            var dto = _mapper.Map<RaspberryDto>(raspberry);

            var url = _urlTemplate.Replace(_placeholderUrlTemplate, request.RpiId.ToString());
            var updaterResponse = await _client.PostAsJsonAsync(url, dto, cancellationToken: cancellationToken);

            GetLoggerActionBasedOnResponseMessage(request.RpiId, updaterResponse)
                ?.Invoke(_logger);

            return response;
        }
    }
}