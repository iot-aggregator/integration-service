﻿using System.IO;
using System.Threading.Tasks;
using IntegrationService.Application.Dtos;

namespace IntegrationService.Application.Services
{
    public interface IBucketService
    {
        /// <summary>
        ///     Retrieves paged list of objects for specified bucket.
        /// </summary>
        /// <param name="bucketName">Storage bucket's name.</param>
        /// <param name="size">Number of objects per page.</param>
        /// <param name="prefix">Filter objects by this value.</param>
        /// <param name="listAfterMarker">List objects starting after this key.</param>
        /// <param name="continuationToken">Continuation token for next page of previous query.</param>
        /// <returns>Data transfer object with next continuation token and list of links for current page.</returns>
        Task<BucketObjectsListDto> GetPagedObjectsFromBucketAsync(string bucketName, int size, string prefix = "",
            string listAfterMarker = "", string continuationToken = "");
        Task<Stream> GetImage(string bucketName, string path);
        Task UploadImage(string bucketName, string key, Stream data);
        Task EnsureBucketCreated();
    }
}