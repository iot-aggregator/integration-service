﻿using System;
using System.Threading.Tasks;

namespace IntegrationService.Application.Services
{
    /// <summary>
    ///     Handles publishing measurements with metadata to configured endpoints.
    /// </summary>
    public interface IMeasurementPublisher
    {
        /// <summary>
        ///     Publishes gauge measurement in configured AppMetrics endpoints.
        /// </summary>
        /// <param name="context">Measurements context.</param>
        /// <param name="name">Measurements name.</param>
        /// <param name="unit">Unit of measurement.</param>
        /// <param name="value">Measured value.</param>
        /// <param name="tags">Optional tags for published metric.</param>
        /// <returns>Completed task.</returns>
        /// <exception cref="ArgumentNullException">
        ///     When <paramref name="context" /> or <paramref name="name" /> or <paramref name="unit" /> is null.
        /// </exception>
        /// <exception cref="ArgumentException">
        ///     When <paramref name="context" /> or <paramref name="name" /> is empty.
        /// </exception>
        Task PublishGaugeMeasurementAsync(
            string context,
            string name,
            string unit,
            double value,
            params (string key, string value)[] tags);
    }
}