﻿using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Dtos;

namespace IntegrationService.Application.Services
{
    public interface IConsulService
    {
        /// <summary>
        ///     Registers service in Consul service discovery.
        /// </summary>
        /// <param name="id">ID of service in service discovery.</param>
        /// <param name="name">Name of service in service discovery.</param>
        /// <param name="host">Host on which service is alive.</param>
        /// <param name="port">Port on which service is alive.</param>
        /// <param name="cancellationToken">Optional cancellation token.</param>
        /// <param name="healthChecks">Health checks for registered service.</param>
        /// <returns>Completed task.</returns>
        Task RegisterServiceAsync(string id, string name, string host, int port,
            CancellationToken cancellationToken = default, params HealthCheckDto[] healthChecks);

        /// <summary>
        ///     Deregisters service from Consul service discovery.
        /// </summary>
        /// <param name="id">ID of service in service discovery.</param>
        /// <param name="cancellationToken">Optional cancellation token.</param>
        /// <returns>Completed task.</returns>
        Task DeregisterServiceAsync(string id, CancellationToken cancellationToken = default);
    }
}