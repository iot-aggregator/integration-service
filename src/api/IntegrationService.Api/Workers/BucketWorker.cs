﻿using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Services;
using Microsoft.Extensions.Hosting;

namespace IntegrationService.Api.Workers
{
    public class BucketWorker : BackgroundService
    {
        private readonly IBucketService _bucketService;

        public BucketWorker(IBucketService bucketService)
        {
            _bucketService = bucketService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await _bucketService.EnsureBucketCreated();
        }
    }
}