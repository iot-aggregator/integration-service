﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using App.Metrics;
using App.Metrics.Gauge;
using IntegrationService.Application.Services;
using Microsoft.Extensions.Hosting;

namespace IntegrationService.Api.Workers
{
    public class PrometheusSamplesWorker : BackgroundService
    {
        private readonly IMeasurementPublisher _measurementPublisher;

        public PrometheusSamplesWorker(IMeasurementPublisher measurementPublisher)
        {
            _measurementPublisher = measurementPublisher;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var random = new Random();

            var temperatureGauge = new GaugeOptions
            {
                Context = "TempContext",
                MeasurementUnit = Unit.Custom("Celsius"),
                Name = "MeasurementName"
            };
            var temperatureGaugeTags = Array.Empty<(string key, string value)>();

            var humidityGauge = new GaugeOptions
            {
                Context = "HumidityContext",
                MeasurementUnit = Unit.Custom("g/m^3"),
                Name = "MeasurementName2",
                Tags = new MetricTags(
                    new[] {"instanceId"},
                    new[] {Guid.NewGuid().ToString()})
            };
            var humidityGaugeTags =
                humidityGauge.Tags.ToDictionary().Select(x => (key: x.Key, value: x.Value)).ToArray();

            while (!stoppingToken.IsCancellationRequested)
            {
                await _measurementPublisher.PublishGaugeMeasurementAsync(temperatureGauge.Context,
                    temperatureGauge.Name,
                    temperatureGauge.MeasurementUnit.Name, random.Next(-20, 20), temperatureGaugeTags);

                await _measurementPublisher.PublishGaugeMeasurementAsync(humidityGauge.Context, humidityGauge.Name,
                    humidityGauge.MeasurementUnit.Name, random.Next(3, 10), humidityGaugeTags);

                await Task.Delay(TimeSpan.FromSeconds(5), stoppingToken);
            }
        }
    }
}