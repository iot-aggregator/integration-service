﻿using System;
using System.Threading;
using System.Threading.Tasks;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Services;
using IntegrationService.Infrastructure.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace IntegrationService.Api.Workers
{
    public class ConsulWorker : BackgroundService
    {
        private readonly IConsulService _consulService;
        private readonly ConsulOptions _options;
        private string _registrationId;

        public ConsulWorker(IOptions<ConsulOptions> options, IConsulService consulService)
        {
            _consulService = consulService;
            _options = options.Value;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _registrationId = $"{_options.ServiceName}-{_options.ServiceId}";

            var healthCheck = new HealthCheckDto
            {
                Name = "Health check",
                Address = $"{_options.ServiceAddress.AbsoluteUri}healthy",
                Method = "GET",
                Interval = TimeSpan.FromSeconds(30),
                Timeout = TimeSpan.FromSeconds(5)
            };

            await _consulService.RegisterServiceAsync(_registrationId, _options.ServiceName,
                _options.ServiceAddress.Host, _options.ServiceAddress.Port, stoppingToken, healthCheck);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await _consulService.DeregisterServiceAsync(_registrationId, cancellationToken);
            await base.StopAsync(cancellationToken);
        }
    }
}