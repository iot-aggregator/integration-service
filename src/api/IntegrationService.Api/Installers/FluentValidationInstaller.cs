﻿using FluentValidation;
using FluentValidation.AspNetCore;
using IntegrationService.Api.Validators.DeviceControllers;
using IntegrationService.Api.Validators.Raspberries;
using IntegrationService.Application.Common;
using IntegrationService.Application.Validators.DeviceControllers;
using IntegrationService.Application.Validators.Raspberries;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;

namespace IntegrationService.Api.Installers
{
    public static class FluentValidationInstaller
    {
        public static IServiceCollection InstallFluentValidation(this IServiceCollection services)
        {
            services.AddFluentValidation(configuration =>
            {
                configuration.DisableDataAnnotationsValidation = true;
                ValidatorOptions.Global.CascadeMode = CascadeMode.Stop;

                configuration.RegisterValidatorsFromAssemblyContaining<CreateRaspberryCommandRequestValidator>();
            });

            return services;
        }
    }
}