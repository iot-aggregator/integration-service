﻿using System.Collections.Generic;
using System.Globalization;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace IntegrationService.Api.Installers
{
    public static class LocalizationInstaller
    {
        public static IServiceCollection InstallLocalization(this IServiceCollection services)
        {
            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("pl-PL");
                options.SetDefaultCulture("pl-PL");
                options.SupportedCultures = options.SupportedUICultures = new List<CultureInfo>
                {
                    new CultureInfo("pl-PL"),
                };
            });

            services.AddLocalization(options =>
            {
                options.ResourcesPath = "Resources";
            });

            return services;
        }

        public static IApplicationBuilder UseLocalization(this IApplicationBuilder app)
        {
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            return app;
        }
    }
}