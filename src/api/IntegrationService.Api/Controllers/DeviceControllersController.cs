﻿using System;
using System.Threading.Tasks;
using IntegrationService.Application.DeviceControllers.Commands.CreateDeviceController;
using IntegrationService.Application.DeviceControllers.Commands.DeleteDeviceController;
using IntegrationService.Application.DeviceControllers.Commands.UpdateDeviceController;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationService.Api.Controllers
{
    [ApiController]
    [Route("raspberries/{rpiId}/[controller]")]
    public class DeviceControllersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public DeviceControllersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> AddAsync(Guid rpiId, CreateDeviceControllerCommand command)
        {
            if (rpiId != command.RpiId)
            {
                return BadRequest("Raspberry Pi IDs mismatch");
            }

            var id = await _mediator.Send(command);

            return StatusCode(201, id);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(Guid rpiId, UpdateDeviceControllerCommand command)
        {
            if (rpiId != command.RpiId)
            {
                return BadRequest("Raspberry Pi IDs mismatch");
            }

            await _mediator.Send(command);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid rpiId, Guid id)
        {
            await _mediator.Send(new DeleteDeviceControllerCommand
            {
                RpiId = rpiId,
                Id = id
            });

            return NoContent();
        }
    }
}