﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Metrics.AspNetCore;
using IntegrationService.Infrastructure.Events;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace IntegrationService.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CacheController : ControllerBase
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IPublishEndpoint _publishEndpoint;

        public CacheController(IMemoryCache memoryCache, IPublishEndpoint publishEndpoint)
        {
            _memoryCache = memoryCache;
            _publishEndpoint = publishEndpoint;
        }

        [HttpDelete("{id}")]
        public IActionResult ClearCacheForStation(Guid id)
        {
            if(!_memoryCache.TryGetValue(id, out _))
            {
                return NotFound();
            }

            _memoryCache.Remove(id);
            return Ok();
        }
    }
}