﻿using System;
using System.Threading.Tasks;
using IntegrationService.Application.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ILogger = Amazon.Runtime.Internal.Util.ILogger;

namespace IntegrationService.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SelfTestController : ControllerBase
    {
        private readonly ILogger<SelfTestController> _logger;

        public SelfTestController(ILogger<SelfTestController> logger)
        {
            _logger = logger;
        }

        [HttpPost("{id}")]
        public IActionResult PostConfigurationAsync(Guid id, RaspberryDto request)
        {
            _logger.LogInformation("Received update request for Id: {RpiId}", id);

            return Ok();
        }
    }
}