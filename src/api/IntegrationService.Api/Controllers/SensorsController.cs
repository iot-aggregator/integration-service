﻿using System;
using System.Threading.Tasks;
using IntegrationService.Application.Sensors.Commands.CreateSensor;
using IntegrationService.Application.Sensors.Commands.DeleteSensor;
using IntegrationService.Application.Sensors.Commands.UpdateSensor;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationService.Api.Controllers
{
    [ApiController]
    [Route("raspberries/{rpiId}/[controller]")]
    public class SensorsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SensorsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> AddAsync(Guid rpiId, CreateSensorCommand command)
        {
            if (rpiId != command.RpiId)
            {
                return BadRequest("Raspberry Pi IDs mismatch");
            }

            var id = await _mediator.Send(command);

            return StatusCode(201, id);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(Guid rpiId, UpdateSensorCommand command)
        {
            if (rpiId != command.RpiId)
            {
                return BadRequest("Raspberry Pi IDs mismatch");
            }

            await _mediator.Send(command);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid rpiId, Guid id)
        {

            await _mediator.Send(new DeleteSensorCommand
            {
                RpiId = rpiId,
                Id = id
            });

            return NoContent();
        }
    }
}