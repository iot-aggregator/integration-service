﻿using System.Threading.Tasks;
using IntegrationService.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationService.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BucketsController : ControllerBase
    {
        private readonly IBucketService _bucketService;

        public BucketsController(IBucketService bucketService)
        {
            _bucketService = bucketService;
        }

        /// <summary>
        ///     Retrieves paged list of objects for specified bucket with continuation token for next page.
        /// </summary>
        /// <param name="bucketName">Storage bucket's name.</param>
        /// <param name="size">Number of objects per page.</param>
        /// <param name="prefix">Filter objects by this value.</param>
        /// <param name="afterMarker">List objects starting after this key.</param>
        /// <param name="continuationToken">Continuation token for next page of previous query.</param>
        /// <returns>Data transfer object with next continuation token and list of links for current page.</returns>
        /// <response code="200">Returns paged list of objects with continuation token for next page.</response>
        [HttpGet("list/{bucketName}")]
        public async Task<IActionResult> ListBucketObjectsAsync(string bucketName, int size = 5, string prefix = "",
            string afterMarker = "", string continuationToken = "")
        {
            var dto = await _bucketService.GetPagedObjectsFromBucketAsync(bucketName, size, prefix, afterMarker,
                continuationToken);

            return Ok(dto);
        }
    }
}