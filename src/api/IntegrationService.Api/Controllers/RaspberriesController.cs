﻿using System;
using System.Threading.Tasks;
using IntegrationService.Application.Raspberries.Commands.CreateRaspberry;
using IntegrationService.Application.Raspberries.Commands.DeleteRaspberry;
using IntegrationService.Application.Raspberries.Commands.UpdateRaspberry;
using IntegrationService.Application.Raspberries.Queries.GetAllRaspberries;
using IntegrationService.Application.Raspberries.Queries.GetRaspberry;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationService.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RaspberriesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RaspberriesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<IActionResult> AddAsync(CreateRaspberryCommand command)
        {
            var id = await _mediator.Send(command);

            return StatusCode(201, id);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync()
        {
            var dtos = await _mediator.Send(new GetAllRaspberriesQuery());

            return Ok(dtos);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var dto = await _mediator.Send(new GetRaspberryQuery
            {
                Id = id
            });

            return Ok(dto);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(Guid id, UpdateRaspberryCommand command)
        {
            if (id != command.RpiId)
            {
                return BadRequest("Raspberry Pi IDs mismatch");
            }

            await _mediator.Send(command);

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            await _mediator.Send(new DeleteRaspberryCommand
            {
                RpiId = id
            });

            return NoContent();
        }
    }
}