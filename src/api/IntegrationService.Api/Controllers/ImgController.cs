﻿using System.IO;
using System.Threading.Tasks;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace IntegrationService.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ImgController : ControllerBase
    {
        private readonly IBucketService _bucketService;

        public ImgController(IBucketService bucketService)
        {
            _bucketService = bucketService;
        }

        [HttpGet("{bucketName}/{**path}")]
        public async Task<IActionResult> GetImageAsync(string bucketName, string path)
        {
            return Ok(await _bucketService.GetImage(bucketName, path));
        }

        [HttpPost("{bucketName}")]
        public async Task<IActionResult> UploadImageAsync([FromForm] ImageUploadDto imageUploadDto, string bucketName)
        {
            await using (var ms = new MemoryStream())
            {
                await imageUploadDto.Image.CopyToAsync(ms);
                await _bucketService.UploadImage(bucketName, imageUploadDto.Key, ms);
            }

            return Accepted();
        }
    }
}