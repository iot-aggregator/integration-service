﻿using System;
using FluentValidation;
using IntegrationService.Application.Common;
using IntegrationService.Application.Repositories;
using IntegrationService.Application.Sensors.Commands.UpdateSensor;
using IntegrationService.Application.Validators.Sensors;
using IntegrationService.Domain.Common;
using Microsoft.Extensions.Localization;

namespace IntegrationService.Api.Validators.Sensors
{
    public class UpdateSensorCommandRequestValidator : AbstractValidator<UpdateSensorCommand>
    {
        public UpdateSensorCommandRequestValidator(IRaspberryRepository raspberryRepository, IStringLocalizer<ApplicationLanguage> localizer)
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty);
            RuleFor(x => x.RpiId)
                .NotEqual(Guid.Empty);
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.PinName)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.Status)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.Type)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.MeasurementType)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.CoefficientA)
                .NotNull();
            RuleFor(x => x.CoefficientB)
                .NotNull();

            Include(new UpdateSensorCommandValidator(raspberryRepository, localizer));
        }
    }
}