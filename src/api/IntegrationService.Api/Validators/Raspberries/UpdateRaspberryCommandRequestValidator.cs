﻿using FluentValidation;
using IntegrationService.Application.Common;
using IntegrationService.Application.Raspberries.Commands.UpdateRaspberry;
using IntegrationService.Application.Repositories;
using IntegrationService.Application.Validators.Raspberries;
using Microsoft.Extensions.Localization;

namespace IntegrationService.Api.Validators.Raspberries
{
    public class UpdateRaspberryCommandRequestValidator : AbstractValidator<UpdateRaspberryCommand>
    {
        public UpdateRaspberryCommandRequestValidator(IRaspberryRepository raspberryRepository, IStringLocalizer<ApplicationLanguage> localizer)
        {
            RuleFor(x => x.Name)
                .NotEmpty();
            RuleFor(x => x.Tube)
                .NotNull();

            Include(new UpdateRaspberryCommandValidator(raspberryRepository, localizer));
        }
    }
}