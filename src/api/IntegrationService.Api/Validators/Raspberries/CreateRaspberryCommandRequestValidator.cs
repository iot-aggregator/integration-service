﻿using FluentValidation;
using IntegrationService.Application.Raspberries.Commands.CreateRaspberry;
using IntegrationService.Application.Validators.Raspberries;

namespace IntegrationService.Api.Validators.Raspberries
{
    public class CreateRaspberryCommandRequestValidator : AbstractValidator<CreateRaspberryCommand>
    {
        public CreateRaspberryCommandRequestValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty();
            RuleFor(x => x.Tube)
                .NotNull();

            Include(new CreateRaspberryCommandValidator());
        }
    }
}