﻿using System;
using System.Linq;
using FluentValidation;
using IntegrationService.Application.Common;
using IntegrationService.Application.DeviceControllers.Commands.UpdateDeviceController;
using IntegrationService.Application.Repositories;
using IntegrationService.Application.Validators.DeviceControllers;
using IntegrationService.Domain.Common;
using Microsoft.Extensions.Localization;

namespace IntegrationService.Api.Validators.DeviceControllers
{
    public class UpdateDeviceControllerCommandRequestValidator : AbstractValidator<UpdateDeviceControllerCommand>
    {
        public UpdateDeviceControllerCommandRequestValidator(IRaspberryRepository raspberryRepository, IStringLocalizer<ApplicationLanguage> localizer)
        {
            RuleFor(x => x.Id)
                .NotEqual(Guid.Empty);
            RuleFor(x => x.RpiId)
                .NotEqual(Guid.Empty);
            RuleFor(x => x.Name)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.PinName)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.Status)
                .NotNull()
                .NotEmpty();
            RuleFor(x => x.ActionTimeInSeconds)
                .NotNull();
            RuleFor(x => x.WaitingTimeInSeconds)
                .NotNull();

            Include(new UpdateDeviceControllerCommandValidator(raspberryRepository, localizer));
        }
    }
}