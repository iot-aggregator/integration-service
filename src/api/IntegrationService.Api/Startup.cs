using System;
using System.Collections.Generic;
using IntegrationService.Api.Configuration;
using IntegrationService.Api.Installers;
using IntegrationService.Api.Middleware;
using IntegrationService.Api.Workers;
using IntegrationService.Application.Common;
using IntegrationService.Application.Installers;
using IntegrationService.Application.Services;
using IntegrationService.Infrastructure.Configuration;
using IntegrationService.Infrastructure.Installers;
using IntegrationService.Infrastructure.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace IntegrationService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var absoluteCacheExpirationInSeconds =
                Environment.GetEnvironmentVariable("ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS");
            if (absoluteCacheExpirationInSeconds is null)
            {
                Environment.SetEnvironmentVariable("ABSOLUTE_CACHE_EXPIRATION_IN_SECONDS", "120");
            }

            services.AddControllers(options =>
            {
                options.Conventions.Add(new RouteTokenTransformerConvention(new SlugifyParameterTransformer()));
            });
            services.AddMemoryCache();
            services.AddHttpClient();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "IntegrationService.Api", Version = "v1" });
            });

            services.InstallLocalization();
            services.InstallFluentValidation();

            var runSampleWorker = Environment.GetEnvironmentVariable("RUN_SAMPLE_WORKER");
            if (runSampleWorker is "1" or null)
            {
                services.AddTransient<IMeasurementPublisher, MeasurementPublisher>();
                services.AddHostedService<PrometheusSamplesWorker>();
            }

            services.AddHttpClient();

            var s3Section = Configuration.GetS3Section();
            if (Environment.GetEnvironmentVariable("USE_CONSUL")?.ToLowerInvariant() == "Y".ToLowerInvariant())
            {
                var consulOptions = Configuration.GetConsulOptions();
                services.Configure<ConsulOptions>(Configuration.GetConsulSection());

                services.InstallConsul(consulOptions);
                services.AddHostedService<ConsulWorker>();

                var rabbitMqHost = consulOptions.GetRabbitMqHostUsingConsulAsync().GetAwaiter().GetResult();
                services.InstallMassTransit(rabbitMqHost);

                services.Configure<S3Options>(options =>
                {
                    Configuration.GetS3Section().Bind(options);
                    options.Host = consulOptions.GetMinIoHostUsingConsulAsync().GetAwaiter().GetResult();
                });
            }
            else
            {
                services.InstallMassTransit(Configuration);

                services.Configure<S3Options>(s3Section);
            }

            services.ConfigureBucketPolicy(s3Section["BucketName"]);
            services.AddHostedService<BucketWorker>();
            services.InstallBucketService(Configuration);

            services.InstallDataAccess(Configuration);
            services.InstallAutoMapper();

            var updaterMode = Environment.GetEnvironmentVariable("RASPBERRY_UPDATER_MODE").GetUpdaterModeByString();
            services.InstallMediatR(updaterMode);

            services.AddHealthChecks();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            var isDevelopment = env.IsDevelopment();
            app.ConfigureExceptionHandler(isDevelopment);

            if (isDevelopment || Environment.GetEnvironmentVariable("USE_SWAGGER")?.ToLowerInvariant() ==
                "Y".ToLowerInvariant())
            {
                var reverseProxyPath = Environment.GetEnvironmentVariable("REVERSE_PROXY_PATH") ?? "";
                app.UseSwagger(options =>
                {
                    options.PreSerializeFilters.Add((swagger, httpReq) =>
                    {
                        if (httpReq.Headers.ContainsKey("X-Forwarded-Host"))
                        {
                            var basePath = reverseProxyPath.TrimStart('/');
                            var serverUrl = $"{httpReq.Scheme}://{httpReq.Headers["X-Forwarded-Host"]}/{basePath}";
                            swagger.Servers = new List<OpenApiServer> { new OpenApiServer { Url = serverUrl } };
                        }
                    });
                });
                app.UseSwaggerUI(c =>
                    c.SwaggerEndpoint($"{reverseProxyPath}/swagger/v1/swagger.json", "IntegrationService.Api v1"));
            }

            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseLocalization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

                endpoints.MapHealthChecks("/healthy", new HealthCheckOptions
                {
                    Predicate = check => check.Tags.Contains("ready")
                });

                endpoints.MapHealthChecks("/live", new HealthCheckOptions
                {
                    Predicate = _ => false
                });
            });
        }
    }
}