﻿using System.Collections.Generic;
using System.Net;
using Amazon.Runtime.Internal;
using IntegrationService.Application.Common;
using IntegrationService.Application.Extensions;
using IntegrationService.Domain.Exceptions;
using IntegrationService.Infrastructure.Exceptions;
using IntegrationService.Infrastructure.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;

namespace IntegrationService.Api.Middleware
{
    public static class ExceptionMiddlewareExtensions
    {
        private class CustomErrorResponse
        {
            public string Title { get; set; }
            public int Status { get; set; }
            public Dictionary<string, string[]> Errors { get; set; }
        }
        private static CustomErrorResponse BuildErrorResponse(this string error, int statusCode, string title = "One or more validation errors occurred.")
        {
            return new CustomErrorResponse
            {
                Status = statusCode,
                Title = title,
                Errors = new Dictionary<string, string[]>
                {
                    {"Error", new[] { error }}
                }
            };
        }

        public static void ConfigureExceptionHandler(this IApplicationBuilder app, bool isDevelopment)
        {
            app.UseExceptionHandler(appError =>
            {
                var error = "Internal Server Error".BuildErrorResponse(500, "Internal server error");
                appError.Run(async context =>
                {
                    var localizer = context.RequestServices.GetRequiredService<IStringLocalizer<ApplicationLanguage>>();
                    var logger = context.RequestServices.GetRequiredService<ILogger<ExceptionMiddleware>>();
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    context.Response.ContentType = "application/json";
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();

                    if (contextFeature == null)
                    {
                        await context.Response.WriteAsJsonAsync(new ErrorResponse
                        {
                            Message = "Internal Server Error"
                        });
                        logger.LogError("Internal server error, no exception was thrown.");

                        return;
                    }

                    if (contextFeature.Error != null)
                    {
                        if (isDevelopment)
                        {
                            error = localizer[contextFeature.Error.Message].Value.BuildErrorResponse(400);
                        }

                        if (contextFeature.Error is BucketServiceException)
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            error = localizer[contextFeature.Error.Message].Value.BuildErrorResponse(context.Response.StatusCode);
                            await context.Response.WriteAsJsonAsync(error);
                            await context.Response.CompleteAsync();
                            return;
                        }

                        // Test Server does not implement CompleteAsync, which is required to allow for 404 response from exception middleware
                        if (contextFeature.Error.IsNotFoundDomainException())
                        {
                            error.Status = context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                            error = localizer[contextFeature.Error.Message].Value.BuildErrorResponse(context.Response.StatusCode);
                            await context.Response.WriteAsJsonAsync(error);
                            await context.Response.CompleteAsync();
                            return;
                        }

                        if (contextFeature.Error.IsDomainException())
                        {
                            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                            error = localizer[contextFeature.Error.Message].Value.BuildErrorResponse(context.Response.StatusCode);
                            await context.Response.WriteAsJsonAsync(error);
                            return;
                        }

                        logger.LogError("{exception} - {exceptionMessage}", contextFeature.Error, contextFeature.Error.Message);
                    }

                    await context.Response.WriteAsJsonAsync(error);
                });
            });
        }
    }
}