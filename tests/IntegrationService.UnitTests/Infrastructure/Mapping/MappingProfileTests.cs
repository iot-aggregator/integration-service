﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Metrics.AspNetCore;
using AutoMapper;
using FluentAssertions;
using IntegrationService.Application.DeviceControllers.Commands.CreateDeviceController;
using IntegrationService.Application.DeviceControllers.Commands.UpdateDeviceController;
using IntegrationService.Application.Dtos;
using IntegrationService.Application.Raspberries.Commands.CreateRaspberry;
using IntegrationService.Application.Raspberries.Commands.UpdateRaspberry;
using IntegrationService.Application.Sensors.Commands.CreateSensor;
using IntegrationService.Application.Sensors.Commands.UpdateSensor;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Mapping;
using Xunit;

namespace IntegrationService.UnitTests.Infrastructure.Mapping
{
    public class MappingProfileTests
    {
        private readonly IMapper _mapper;

        public MappingProfileTests()
        {
            var mapperConfiguration = new MapperConfiguration(expression =>
            {
                expression.AddProfile<MappingProfile>();
            });

            _mapper = mapperConfiguration.CreateMapper();
        }

        [Fact]
        public void
            MappingCreateDeviceControllerCommandToDeviceController_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var dto = new CreateDeviceControllerCommand
            {
                RpiId = Guid.NewGuid(),
                PinName = "GPIO1",
                Name = "Name1",
                Description = "Desc1",
                Status = "active",
                ActionTimeInSeconds = 25,
                WaitingTimeInSeconds = 15
            };

            var controller = new DeviceController(
                Guid.NewGuid(),
                dto.RpiId,
                dto.PinName,
                dto.Name,
                dto.Description,
                PinStatus.Active,
                TimeSpan.FromSeconds(dto.WaitingTimeInSeconds),
                TimeSpan.FromSeconds(dto.ActionTimeInSeconds)
            );

            // Act
            var mapped = _mapper.Map<DeviceController>(dto);

            // Assert
            mapped.Should().BeEquivalentTo(controller, options =>
                options.Excluding(x => x.Id));
        }

        [Fact]
        public void MappingCreateRaspberryCommandToRaspberry_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var dto = new CreateRaspberryCommand
            {
                Name = "Name1",
                Tube = 1
            };

            var raspberry = new Raspberry(
                Guid.NewGuid(),
                dto.Name,
                dto.Tube);

            // Act
            var mapped = _mapper.Map<Raspberry>(dto);

            mapped.Should().BeEquivalentTo(raspberry, options =>
                options.Excluding(x => x.Id));
        }

        [Fact]
        public void MappingCreateSensorCommandToSensor_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var dto = new CreateSensorCommand()
            {
                RpiId = Guid.NewGuid(),
                PinName = "GPIO1",
                Name = "Name1",
                Description = "Desc1",
                Status = "inactive",
                Type = "digital",
                MeasurementType = "C",
                CoefficientA = 11,
                CoefficientB = -0.5
            };

            var sensor = new Sensor(
                Guid.NewGuid(),
                dto.RpiId,
                dto.PinName,
                dto.Name,
                dto.Description,
                PinStatus.Inactive,
                dto.MeasurementType,
                SensorType.Digital,
                dto.CoefficientA,
                dto.CoefficientB);

            // Act
            var mapped = _mapper.Map<Sensor>(dto);

            mapped.Should().BeEquivalentTo(sensor, options =>
                options.Excluding(x => x.Id));
        }

        [Fact]
        public void
            MappingUpdateDeviceControllerCommandToDeviceController_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var dto = new UpdateDeviceControllerCommand
            {
                Id = Guid.NewGuid(),
                RpiId = Guid.NewGuid(),
                PinName = "GPIO1",
                Name = "Name1",
                Description = "Desc1",
                Status = "active",
                ActionTimeInSeconds = 25,
                WaitingTimeInSeconds = 15
            };

            var controller = new DeviceController(
                dto.Id,
                dto.RpiId,
                dto.PinName,
                dto.Name,
                dto.Description,
                PinStatus.Active,
                TimeSpan.FromSeconds(dto.WaitingTimeInSeconds),
                TimeSpan.FromSeconds(dto.ActionTimeInSeconds)
            );

            // Act
            var mapped = _mapper.Map<DeviceController>(dto);

            // Assert
            mapped.Should().BeEquivalentTo(controller);
        }

        [Fact]
        public void MappingUpdateRaspberryCommandToRaspberry_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var dto = new UpdateRaspberryCommand
            {
                RpiId = Guid.NewGuid(),
                Name = "Name1",
                Tube = 1
            };

            var raspberry = new Raspberry(
                dto.RpiId,
                dto.Name,
                dto.Tube);

            // Act
            var mapped = _mapper.Map<Raspberry>(dto);

            mapped.Should().BeEquivalentTo(raspberry);
        }

        [Fact]
        public void MappingUpdateSensorCommandToSensor_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var dto = new UpdateSensorCommand
            {
                Id = Guid.NewGuid(),
                RpiId = Guid.NewGuid(),
                PinName = "GPIO1",
                Name = "Name1",
                Description = "Desc1",
                Status = "inactive",
                Type = "digital",
                MeasurementType = "C",
                CoefficientA = 11,
                CoefficientB = -0.5
            };

            var sensor = new Sensor(
                dto.Id,
                dto.RpiId,
                dto.PinName,
                dto.Name,
                dto.Description,
                PinStatus.Inactive,
                dto.MeasurementType,
                SensorType.Digital,
                dto.CoefficientA,
                dto.CoefficientB);

            // Act
            var mapped = _mapper.Map<Sensor>(dto);

            mapped.Should().BeEquivalentTo(sensor);
        }

        [Fact]
        public void
            MappingDeviceControllerToDeviceControllerDto_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var controller = new DeviceController(
                Guid.NewGuid(),
                Guid.NewGuid(),
                "GPIO1",
                "Name1",
                "Desc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(25),
                TimeSpan.FromSeconds(15)
            );

            var dto = new DeviceControllerDto
            {
                Id = controller.Id,
                PinName = controller.PinName,
                Name = controller.Name,
                Description = controller.Description,
                Status = controller.PinStatus.ToString().ToLowerInvariant(),
                ActionTimeInSeconds = (int)controller.ActionTime.TotalSeconds,
                WaitingTimeInSeconds = (int)controller.WaitingTime.TotalSeconds
            };

            // Act
            var mapped = _mapper.Map<DeviceControllerDto>(controller);

            // Assert
            mapped.Should().BeEquivalentTo(dto);
        }

        [Fact]
        public void MappingSensorToSensorDto_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var sensor = new Sensor
            (
                Guid.NewGuid(),
                Guid.NewGuid(),
                "GPIO1",
                "Name1",
                "Desc1",
                PinStatus.Inactive,
                "C",
                SensorType.Digital,
                11,
                -0.5
            );

            var dto = new SensorDto
            {
                Id = sensor.Id,
                Name = sensor.Name,
                Description = sensor.Description,
                Status = sensor.PinStatus.ToString().ToLowerInvariant(),
                MeasurementType = sensor.MeasurementType,
                CoefficientA = sensor.CoefficientA,
                CoefficientB = sensor.CoefficientB,
                PinName = sensor.PinName,
                SensorType = sensor.Type.ToString().ToLowerInvariant()
            };

            // Act
            var mapped = _mapper.Map<SensorDto>(sensor);

            mapped.Should().BeEquivalentTo(dto);
        }

        [Fact]
        public void MappingRaspberryToRaspberryBlockDto_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "Name1",
                1);

            var sensor = new Sensor
            (
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "Name1",
                "Desc1",
                PinStatus.Inactive,
                "C",
                SensorType.Digital,
                11,
                -0.5
            );
            raspberry.AddSensor(sensor);

            var controller = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "Name2",
                "Desc2",
                PinStatus.Active,
                TimeSpan.FromSeconds(25),
                TimeSpan.FromSeconds(15)
            );
            raspberry.AddController(controller);

            var dto = new RaspberryBlockDto
            {
                Id = raspberry.Id,
                Name = raspberry.Name,
                Tube = raspberry.Tube
            };

            // Act
            var mapped = _mapper.Map<RaspberryBlockDto>(raspberry);

            mapped.Should().BeEquivalentTo(dto);
        }

        [Fact]
        public void MappingRaspberryToRaspberryDto_ShouldReturnMappedDeviceControllerWithEqualValues()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "Name1",
                1);

            var sensor = new Sensor
            (
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "Name1",
                "Desc1",
                PinStatus.Inactive,
                "C",
                SensorType.Digital,
                11,
                -0.5
            );
            var sensorDto = new SensorDto
            {
                Id = sensor.Id,
                Name = sensor.Name,
                Description = sensor.Description,
                Status = sensor.PinStatus.ToString().ToLowerInvariant(),
                MeasurementType = sensor.MeasurementType,
                CoefficientA = sensor.CoefficientA,
                CoefficientB = sensor.CoefficientB,
                PinName = sensor.PinName,
                SensorType = sensor.Type.ToString().ToLowerInvariant()
            };
            raspberry.AddSensor(sensor);

            var controller = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "Name2",
                "Desc2",
                PinStatus.Active,
                TimeSpan.FromSeconds(25),
                TimeSpan.FromSeconds(15)
            );
            var controllerDto = new DeviceControllerDto
            {
                Id = controller.Id,
                PinName = controller.PinName,
                Name = controller.Name,
                Description = controller.Description,
                Status = controller.PinStatus.ToString().ToLowerInvariant(),
                ActionTimeInSeconds = (int)controller.ActionTime.TotalSeconds,
                WaitingTimeInSeconds = (int)controller.WaitingTime.TotalSeconds
            };
            raspberry.AddController(controller);

            var dto = new RaspberryDto
            {
                Id = raspberry.Id,
                Name = raspberry.Name,
                Tube = raspberry.Tube,
                Config = new RaspberryConfigurationDto
                {
                    Controllers = new List<DeviceControllerDto>
                    {
                        controllerDto
                    },
                    Sensors = new List<SensorDto>
                    {
                        sensorDto
                    }
                }
            };

            // Act
            var mapped = _mapper.Map<RaspberryDto>(raspberry);

            mapped.Should().BeEquivalentTo(dto);
        }
    }
}