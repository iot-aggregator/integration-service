using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using FluentAssertions;
using IntegrationService.Domain.Common;
using IntegrationService.Domain.Entities;
using IntegrationService.Infrastructure.Configuration;
using IntegrationService.Infrastructure.Persistence;
using IntegrationService.Infrastructure.Persistence.Repositories;
using IntegrationService.IntegrationTests.Helpers;
using Microsoft.Extensions.Options;
using Xunit;

namespace IntegrationService.IntegrationTests.Infrastructure.Repositories
{
    public class SqlRaspberryRepositoryTests : IClassFixture<DatabaseFixture>, IDisposable
    {
        private readonly SqlRaspberryRepository _sut;

        public SqlRaspberryRepositoryTests(DatabaseFixture databaseFixture)
        {
            var sqlOptions = Options.Create(new SqlOptions
            {
                ConnectionString =
                    $"Server=localhost;Port={DatabaseFixture.ExternalDatabasePort};Database=iotAggregatorDB;User Id=postgres;Password={DatabaseFixture.DbPassword};Pooling=false;Timeout = 300;CommandTimeout = 300"
            });
            var connectionFactory = new SqlConnectionFactory(sqlOptions);
            _sut = new SqlRaspberryRepository(connectionFactory);
        }

        [Fact]
        public async Task AddRaspberryAsync_ShouldCreateRecordInDatabase_WhenAllPropertiesAreValid()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                Array.Empty<DeviceController>(),
                Array.Empty<Sensor>());

            // Act
            await _sut.AddRaspberryAsync(raspberry);

            // Assert
            var retrievedRaspberry = await _sut.GetRaspberryAsync(raspberry.Id);
            retrievedRaspberry.Should().BeEquivalentTo(raspberry);
        }

        [Fact]
        public async Task GetRaspberriesAsync_ShouldNotReturnNull()
        {
            // Act
            var raspberries = await _sut.GetRaspberriesAsync();

            // Assert
            raspberries.Should().NotBeNull();
        }

        [Fact]
        public async Task GetRaspberriesAsync_ShouldReturnEmptyEnumerable_WhenNoRaspberriesExistInDatabase()
        {
            // Act
            var raspberries = await _sut.GetRaspberriesAsync();

            // Assert
            raspberries.Should().BeEmpty();
        }

        [Fact]
        public async Task UpdateRaspberryAsync_ShouldUpdateEntityWithNewValues_WhenAllPropertiesAreValid()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                Array.Empty<DeviceController>(),
                Array.Empty<Sensor>());

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var updatedRaspberry = new Raspberry(
                raspberry.Id,
                $"{raspberry.Name}-new",
                2,
                raspberry.Controllers
                , raspberry.Sensors);

            await _sut.UpdateRaspberryAsync(updatedRaspberry);

            // Arrange
            var retrievedRaspberry = await _sut.GetRaspberryAsync(raspberry.Id);

            retrievedRaspberry.Should().NotBeEquivalentTo(raspberry);
            retrievedRaspberry.Should().BeEquivalentTo(updatedRaspberry);
        }

        [Fact]
        public async Task DeleteRaspberryAsync_ShouldDeleteRaspberry_WhenRaspberryExistsInDatabase()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                Array.Empty<DeviceController>(),
                Array.Empty<Sensor>());

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            await _sut.DeleteRaspberryAsync(raspberry.Id);

            // Assert
            var retrievedRaspberry = await _sut.GetRaspberryAsync(raspberry.Id);

            retrievedRaspberry.Should().BeNull();
        }

        [Fact]
        public async Task AddSensorAsync_ShouldThrowDbException_WhenRaspberryDoesNotExist()
        {
            // Arrange
            var sensor = new Sensor(
                Guid.NewGuid(),
                Guid.NewGuid(),
                "ANALOG1",
                "SensorName1",
                "SensorDesc1",
                PinStatus.Active,
                "C",
                SensorType.Analog,
                1.5,
                2.5);

            Func<Task> func = async () =>
            {
                // Act
                await _sut.AddSensorAsync(sensor);
            };

            // Assert
            await func.Should().ThrowAsync<DbException>();
        }

        [Fact]
        public async Task AddSensorAsync_ShouldCreateRecordInDatabase_WhenRaspberryExistsAndAllPropertiesAreValid()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var sensor = new Sensor(
                Guid.NewGuid(),
                raspberry.Id,
                "ANALOG1",
                "SensorName1",
                "SensorDesc1",
                PinStatus.Active,
                "C",
                SensorType.Analog,
                1.5,
                2.5);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            await _sut.AddSensorAsync(sensor);

            // Assert
            var retrievedSensor = await _sut.GetSensorAsync(sensor.Id);
            retrievedSensor.Should().BeEquivalentTo(sensor);
        }

        [Fact]
        public async Task GetSensorAsync_ShouldReturnNull_WhenSensorDoesNotExistInDatabase()
        {
            // Act
            var sensor = await _sut.GetSensorAsync(Guid.NewGuid());

            // Assert
            sensor.Should().BeNull();
        }

        [Fact]
        public async Task GetSensorAsync_ShouldNotReturnNull_WhenSensorExistsInDatabase()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var sensor = new Sensor(
                Guid.NewGuid(),
                raspberry.Id,
                "ANALOG1",
                "SensorName1",
                "SensorDesc1",
                PinStatus.Active,
                "C",
                SensorType.Analog,
                1.5,
                2.5);

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddSensorAsync(sensor);

            // Act
            var retrievedSensor = await _sut.GetSensorAsync(sensor.Id);

            // Assert
            retrievedSensor.Should().NotBeNull();
        }

        [Fact]
        public async Task GetSensorAsync_ShouldReturnSensor_WhenSensorExistsInDatabase()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var sensor = new Sensor(
                Guid.NewGuid(),
                raspberry.Id,
                "ANALOG1",
                "SensorName1",
                "SensorDesc1",
                PinStatus.Active,
                "C",
                SensorType.Analog,
                1.5,
                2.5);

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddSensorAsync(sensor);

            // Act
            var retrievedSensor = await _sut.GetSensorAsync(sensor.Id);

            // Assert
            retrievedSensor.Should().BeEquivalentTo(sensor);
        }

        [Fact]
        public async Task GetRaspberrySensorsAsync_ShouldNotReturnNull_WhenRaspberryDoesNotExistInDatabase()
        {
            // Arrange
            var retrievedSensors = await _sut.GetRaspberrySensorsAsync(Guid.NewGuid());

            // Assert
            retrievedSensors.Should().NotBeNull();
        }

        [Fact]
        public async Task GetRaspberrySensorsAsync_ShouldReturnEmptyEnumerable_WhenRaspberryDoesNotExistInDatabase()
        {
            // Arrange
            var retrievedSensors = await _sut.GetRaspberrySensorsAsync(Guid.NewGuid());

            // Assert
            retrievedSensors.Should().BeEmpty();
        }

        [Fact]
        public async Task GetRaspberrySensorsAsync_ShouldNotReturnNull_WhenRaspberryDoesNotHaveAnySensors()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedSensors = await _sut.GetRaspberrySensorsAsync(raspberry.Id);

            // Assert
            retrievedSensors.Should().NotBeNull();
        }

        [Fact]
        public async Task GetRaspberrySensorsAsync_ShouldReturnEmptyEnumerable_WhenRaspberryDoesNotHaveAnySensors()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedSensors = await _sut.GetRaspberrySensorsAsync(raspberry.Id);

            // Assert
            retrievedSensors.Should().BeEmpty();
        }

        [Fact]
        public async Task GetRaspberrySensorsAsync_ShouldReturnAllRaspberrySensors()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var sensors = new List<Sensor>()
            {
                new Sensor(
                    Guid.NewGuid(),
                    raspberry.Id,
                    "ANALOG1",
                    "SensorName1",
                    "SensorDesc1",
                    PinStatus.Active,
                    "C",
                    SensorType.Analog,
                    1.5,
                    2.5),
                new Sensor(
                    Guid.NewGuid(),
                    raspberry.Id,
                    "ANALOG2",
                    "SensorName2",
                    "SensorDesc2",
                    PinStatus.Active,
                    "C",
                    SensorType.Analog,
                    3.5,
                    4.255)
            };

            await _sut.AddRaspberryAsync(raspberry);
            foreach (var sensor in sensors)
            {
                await _sut.AddSensorAsync(sensor);
            }

            // Act
            var retrievedSensors = await _sut.GetRaspberrySensorsAsync(raspberry.Id);

            // Assert
            retrievedSensors.Should().BeEquivalentTo(sensors);
        }

        [Fact]
        public async Task UpdateSensorAsync_ShouldUpdateEntityWithNewValues_WhenAllPropertiesAreValid()
        {
            // Arrange 
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var sensor = new Sensor(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "SensorName1",
                "SensorDesc1",
                PinStatus.Active,
                "C",
                SensorType.Digital,
                1.256,
                2);

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddSensorAsync(sensor);

            // Act
            var updatedSensor = new Sensor(
                sensor.Id,
                sensor.RaspberryId,
                $"{sensor.PinName.Substring(0, sensor.PinName.Length - 1)}2",
                $"{sensor.Name}-new",
                $"{sensor.Description}-new",
                PinStatus.Inactive,
                "D",
                SensorType.Analog,
                5,
                2.523
            );

            await _sut.UpdateSensorAsync(updatedSensor);

            // Assert
            var retrievedSensor = await _sut.GetSensorAsync(sensor.Id);

            retrievedSensor.Should().NotBeEquivalentTo(sensor);
            retrievedSensor.Should().BeEquivalentTo(updatedSensor);
        }

        [Fact]
        public async Task DeleteDeviceAsync_ShouldDeleteSensor_WhenSensorExistsInDatabase()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var sensor = new Sensor(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "SensorName1",
                "SensorDesc1",
                PinStatus.Active,
                "C",
                SensorType.Digital,
                1.256,
                2);

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddSensorAsync(sensor);

            // Act
            await _sut.DeleteDeviceAsync(sensor.Id);

            // Assert
            var retrievedSensor = await _sut.GetSensorAsync(sensor.Id);

            retrievedSensor.Should().BeNull();
        }

        [Fact]
        public async Task AddDeviceControllerAsync_ShouldThrowDbException_WhenRaspberryDoesNotExist()
        {
            // Arrange
            var controller = new DeviceController(
                Guid.NewGuid(),
                Guid.NewGuid(),
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(15),
                TimeSpan.FromSeconds(2)
            );

            Func<Task> func = async () =>
            {
                // Act
                await _sut.AddDeviceControllerAsync(controller);
            };

            // Assert
            await func.Should().ThrowAsync<DbException>();
        }

        [Fact]
        public async Task AddDeviceControllerAsync_ShouldCreateRecordInDatabase_WhenRaspberryExistsAndAllPropertiesAreValid()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var controller = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(15),
                TimeSpan.FromSeconds(2)
            );

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            await _sut.AddDeviceControllerAsync(controller);

            // Assert
            var retrievedDeviceController = await _sut.GetDeviceControllerAsync(controller.Id);
            retrievedDeviceController.Should().BeEquivalentTo(controller);
        }

        [Fact]
        public async Task GetDeviceControllerAsync_ShouldReturnNull_WhenDeviceControllerDoesNotExistInDatabase()
        {
            // Act
            var controller = await _sut.GetDeviceControllerAsync(Guid.NewGuid());

            // Assert
            controller.Should().BeNull();
        }

        [Fact]
        public async Task GetDeviceControllerAsync_ShouldNotReturnNull_WhenDeviceControllerExistsInDatabase()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var controller = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(15),
                TimeSpan.FromSeconds(2)
            );

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddDeviceControllerAsync(controller);

            // Act
            var retrievedDeviceController = await _sut.GetDeviceControllerAsync(controller.Id);

            // Assert
            retrievedDeviceController.Should().NotBeNull();
        }

        [Fact]
        public async Task GetDeviceControllerAsync_ShouldReturnDeviceController_WhenDeviceControllerExistsInDatabase()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var controller = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(15),
                TimeSpan.FromSeconds(2)
            );

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddDeviceControllerAsync(controller);

            // Act
            var retrievedDeviceController = await _sut.GetDeviceControllerAsync(controller.Id);

            // Assert
            retrievedDeviceController.Should().BeEquivalentTo(controller);
        }

        [Fact]
        public async Task GetRaspberryDeviceControllersAsync_ShouldNotReturnNull_WhenRaspberryDoesNotExistInDatabase()
        {
            // Act
            var retrievedDeviceControllers = await _sut.GetRaspberryDeviceControllersAsync(Guid.NewGuid());

            // Assert
            retrievedDeviceControllers.Should().NotBeNull();
        }

        [Fact]
        public async Task GetRaspberryDeviceControllersAsync_ShouldReturnEmptyEnumerable_WhenRaspberryDoesNotExistInDatabase()
        {
            // Act
            var retrievedDeviceControllers = await _sut.GetRaspberryDeviceControllersAsync(Guid.NewGuid());

            // Assert
            retrievedDeviceControllers.Should().BeEmpty();
        }

        [Fact]
        public async Task GetRaspberryDeviceControllersAsync_ShouldNotReturnNull_WhenRaspberryDoesNotHaveAnyDeviceControllers()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedDeviceControllers = await _sut.GetRaspberryDeviceControllersAsync(raspberry.Id);

            // Assert
            retrievedDeviceControllers.Should().NotBeNull();
        }

        [Fact]
        public async Task GetRaspberryDeviceControllersAsync_ShouldReturnEmptyEnumerable_WhenRaspberryDoesNotHaveAnyDeviceControllers()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedDeviceControllers = await _sut.GetRaspberryDeviceControllersAsync(raspberry.Id);

            // Assert
            retrievedDeviceControllers.Should().BeEmpty();
        }

        [Fact]
        public async Task GetRaspberryDeviceControllersAsync_ShouldReturnAllRaspberryDeviceControllers()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var deviceControllers = new List<DeviceController>()
            {
                new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(15),
                TimeSpan.FromSeconds(2)
                ),
                new DeviceController(
                    Guid.NewGuid(),
                    raspberry.Id,
                    "GPIO2",
                    "ControllerName2",
                    "ControllerDesc2",
                    PinStatus.Active,
                    TimeSpan.FromSeconds(255),
                    TimeSpan.FromSeconds(5)
                )
        };

            await _sut.AddRaspberryAsync(raspberry);
            foreach (var deviceController in deviceControllers)
            {
                await _sut.AddDeviceControllerAsync(deviceController);
            }

            // Act
            var raspberryDeviceControllers = await _sut.GetRaspberryDeviceControllersAsync(raspberry.Id);

            // Assert
            raspberryDeviceControllers.Should().BeEquivalentTo(deviceControllers);
        }

        [Fact]
        public async Task UpdateDeviceControllerAsync_ShouldUpdateEntityWithNewValues_WhenAllPropertiesAreValid()
        {
            // Arrange 
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var deviceController = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(120),
                TimeSpan.FromSeconds(10)
            );

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddDeviceControllerAsync(deviceController);

            // Act
            var updatedDeviceController = new DeviceController(
                deviceController.Id,
                deviceController.RaspberryId,
                $"{deviceController.PinName.Substring(0, deviceController.PinName.Length - 1)}2",
                $"{deviceController.Name}-new",
                $"{deviceController.Description}-new",
                PinStatus.Inactive,
                deviceController.WaitingTime.Subtract(TimeSpan.FromSeconds(50)),
                deviceController.ActionTime.Add(TimeSpan.FromSeconds(3)));

            await _sut.UpdateDeviceControllerAsync(updatedDeviceController);

            // Assert
            var retrievedDeviceController = await _sut.GetDeviceControllerAsync(deviceController.Id);

            retrievedDeviceController.Should().NotBeEquivalentTo(deviceController);
            retrievedDeviceController.Should().BeEquivalentTo(updatedDeviceController);
        }

        [Fact]
        public async Task DeleteDeviceAsync_ShouldDeleteDeviceController_WhenDeviceControllerExistsInDatabase()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var deviceController = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(120),
                TimeSpan.FromSeconds(10)
            );

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddDeviceControllerAsync(deviceController);

            // Act
            await _sut.DeleteDeviceAsync(deviceController.Id);

            // Assert
            var retrievedDeviceController = await _sut.GetDeviceControllerAsync(deviceController.Id);

            retrievedDeviceController.Should().BeNull();
        }

        [Fact]
        public async Task GetRaspberryWithDevices_ShouldReturnNull_WhenRaspberryDoesNotExistInDatabase()
        {
            // Act
            var retrievedRaspberry = await _sut.GetRaspberryWithDevicesAsync(Guid.NewGuid());

            // Assert
            retrievedRaspberry.Should().BeNull();
        }

        [Fact]
        public async Task
            GetRaspberryWithDevices_ShouldReturnRaspberryWithSensorsCollectionNotNull_WhenRaspberryExistsInDatabaseAndHasNoSensors()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedRaspberry = await _sut.GetRaspberryWithDevicesAsync(raspberry.Id);

            // Assert
            retrievedRaspberry.Sensors.Should().NotBeNull();
        }

        [Fact]
        public async Task
            GetRaspberryWithDevices_ShouldReturnRaspberryWithEmptySensorsCollection_WhenRaspberryExistsInDatabaseAndHasNoSensors()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedRaspberry = await _sut.GetRaspberryWithDevicesAsync(raspberry.Id);

            // Assert
            retrievedRaspberry.Sensors.Should().BeEmpty();
        }

        [Fact]
        public async Task
            GetRaspberryWithDevices_ShouldReturnRaspberryWithDeviceControllersCollectionNotNull_WhenRaspberryExistsInDatabaseAndHasNoDeviceControllers()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedRaspberry = await _sut.GetRaspberryWithDevicesAsync(raspberry.Id);

            // Assert
            retrievedRaspberry.Controllers.Should().NotBeNull();
        }

        [Fact]
        public async Task
            GetRaspberryWithDevices_ShouldReturnRaspberryWithEmptyDeviceControllersCollection_WhenRaspberryExistsInDatabaseAndHasNoDeviceControllers()
        {
            // Arrange
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            await _sut.AddRaspberryAsync(raspberry);

            // Act
            var retrievedRaspberry = await _sut.GetRaspberryWithDevicesAsync(raspberry.Id);

            // Assert
            retrievedRaspberry.Controllers.Should().BeEmpty();
        }

        [Fact]
        public async Task GetRaspberryWithDevicesAsync_ShouldReturnRaspberryWithItsDevicesInCollections_WhenAllPropertiesAreValid()
        {
            var raspberry = new Raspberry(
                Guid.NewGuid(),
                "TestName1",
                1,
                null,
                null);

            var sensor = new Sensor(
                Guid.NewGuid(),
                raspberry.Id,
                "ANALOG1",
                "SensorName1",
                "SensorDesc1",
                PinStatus.Active,
                "C",
                SensorType.Analog,
                1.5,
                2.5);
            raspberry.AddSensor(sensor);

            var controller = new DeviceController(
                Guid.NewGuid(),
                raspberry.Id,
                "GPIO1",
                "ControllerName1",
                "ControllerDesc1",
                PinStatus.Active,
                TimeSpan.FromSeconds(15),
                TimeSpan.FromSeconds(2)
            );
            raspberry.AddController(controller);

            await _sut.AddRaspberryAsync(raspberry);
            await _sut.AddSensorAsync(sensor);
            await _sut.AddDeviceControllerAsync(controller);

            // Act
            var retrievedRaspberry = await _sut.GetRaspberryWithDevicesAsync(raspberry.Id);

            // Assert
            retrievedRaspberry.Should().BeEquivalentTo(raspberry);
        }

        public void Dispose()
        {
            _sut.ClearDatabase().GetAwaiter().GetResult();
            GC.SuppressFinalize(this);
        }
    }
}
