﻿using System;
using Ductus.FluentDocker.Services;

namespace IntegrationService.IntegrationTests.Helpers
{
    public abstract class ContainerFixture : IDisposable
    {
        protected IContainerService Container;
        public abstract void CreateContainer();

        public void Dispose()
        {
            Container.StopOnDispose = true;
            Container.RemoveOnDispose = true;
            Container.Dispose();
        }
    }
}