﻿using System;
using Ductus.FluentDocker.Builders;
using Ductus.FluentDocker.Services;

namespace IntegrationService.IntegrationTests.Helpers
{
    public class DatabaseFixture : IDisposable
    {
        private readonly IContainerService _container;
        public const string DbPassword = "passw0rd";
        public const int ExternalDatabasePort = 26555;
        public DatabaseFixture()
        {
            _container = new Builder()
                .UseContainer()
                .UseImage("registry.gitlab.com/iot-aggregator/database")
                .ExposePort(ExternalDatabasePort, 5432)
                .WithEnvironment($"POSTGRES_PASSWORD={DbPassword}")
                .WaitForPort("5432/tcp", TimeSpan.FromSeconds(30))
                .HealthCheck("pg_isready")
                .UseNetwork(DockerConsts.NetworkName)
                .WaitForHealthy()
                .Build()
                .Start();
        }

        public void Dispose()
        {
            _container.StopOnDispose = true;
            _container.RemoveOnDispose = true;
            _container.Dispose();
        }
    }
}