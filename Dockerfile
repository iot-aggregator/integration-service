FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine AS build
WORKDIR /app
COPY ./src ./src
RUN dotnet restore ./src/api/IntegrationService.Api/IntegrationService.Api.csproj
RUN dotnet publish ./src/api/IntegrationService.Api/IntegrationService.Api.csproj -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app/out .
EXPOSE 80
EXPOSE 443
ENTRYPOINT dotnet IntegrationService.Api.dll